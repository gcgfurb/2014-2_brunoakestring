package br.com.viseduandroid.notification;

/**
 * Classe responsavel por manter as variaveis globais do GCM CCS
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 17/09/2014
 * @version 1.0
 */
public class Globals {

	public static final String TAG = "GCM DEMO";

    public static final String GCM_SENDER_ID = "914807375209";
    
    public static final String PREFS_NAME = "VISEDUANDROID";
    public static final String PREFS_PROPERTY_REG_ID = "registration_id";
    
    public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks
	
}



