/**
 * 
 */
package br.com.viseduandroid.notification.broadcastreceiver;

import br.com.viseduandroid.notification.intentservice.GcmIntentService;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * Classe responsavel por implementar o comportamento de criar wake lock
 * para manter o aparelho "acordado" enquanto recebe a notificacao
 * 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 17/09/2014
 * @version 1.0
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// Explicitly specify that GcmIntentService will handle the intent.
		ComponentName comp = new ComponentName(context.getPackageName(),
				GcmIntentService.class.getName());
		// Start the service, keeping the device awake while it is launching.
		startWakefulService(context, (intent.setComponent(comp)));
		setResultCode(Activity.RESULT_OK);
	}
}


