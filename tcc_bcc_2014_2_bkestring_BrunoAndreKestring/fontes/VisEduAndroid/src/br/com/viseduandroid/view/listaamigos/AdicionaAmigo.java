package br.com.viseduandroid.view.listaamigos;

import java.util.List;

import br.com.viseduandroid.R;
import br.com.viseduandroid.adapter.UsuarioAdapter;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.webserviceclient.rest.usuario.UsuarioREST;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class AdicionaAmigo extends ActionBarActivity {

	private Usuario usuario;
	private List<Usuario> listaUsuarios;

	// private final ListView listView = new ListView(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_adiciona_amigo);

		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(ListaAmigosActivity.MESSAGE)) {
			usuario = (Usuario) intent
					.getSerializableExtra(ListaAmigosActivity.MESSAGE);
		}

		if (usuario != null) {
			try {
				listaUsuarios = new UsuarioREST().getUsuarioNaoAmigo(usuario
						.getCodigo());
				Log.d("TESTE", "Lista SIZE = " + listaUsuarios.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		ListView listView = (ListView) findViewById(R.id.listaUsuarios);
		if (listaUsuarios != null) {
			listView.requestFocus();
			listView.setAdapter(new UsuarioAdapter(this, usuario, listaUsuarios));
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.adiciona_amigo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
