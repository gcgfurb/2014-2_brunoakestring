package br.com.viseduandroid.view.listapontos;

import java.util.List;

import br.com.viseduandroid.R;
import br.com.viseduandroid.R.id;
import br.com.viseduandroid.adapter.PontoLocalAdapter;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseLocal;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.listaamigos.ListaAmigosActivity;
import br.com.viseduandroid.view.map.MapaActivity;
import br.com.viseduandroid.view.ponto.PontoLocalActivity;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseLocalREST;
import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class ListaPontosLocaisActivity extends ActionBarActivity {

	private Usuario usuario;
	private List<PontoInteresseLocal> listaPontosLocais;
	private PontoLocalAdapter adapter;
	private PontoInteresseLocalREST pontoRest;
	
	
	public static final String MESSAGE = "ListaPontosLocaisActivity";
	public static final String MESSAGE_USUARIO = "ListaPontosLocaisActivityUsuario";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_pontos_locais);
		
		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(MapaActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(MapaActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaAmigosActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaAmigosActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaPontosGlobaisActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaPontosGlobaisActivity.MESSAGE);
		}
		
		pontoRest = new PontoInteresseLocalREST();
		
		if(usuario != null){
			try {
				listaPontosLocais = pontoRest.getTodosPontosLocais(
						usuario.getCodigo());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		adapter = new PontoLocalAdapter(this, listaPontosLocais);
		
		if(listaPontosLocais!= null){
			ListView listView = (ListView) findViewById(R.id.listViewPontosLocais);
			listView.requestFocus();
			listView.setAdapter(adapter);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_pontos_locais, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		Intent intent;
		switch (item.getItemId()) {
		case id.listaPontoGlobal:
			intent = new Intent(this, ListaPontosGlobaisActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		case id.mapa:
			intent = new Intent(this, MapaActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		case id.listaAmigos:
			intent = new Intent(this, ListaAmigosActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void editarPontoLocal(View view){
		PontoInteresseLocal ponto = (PontoInteresseLocal) view.getTag();
		Intent intent = new Intent(this, PontoLocalActivity.class);
		if (ponto != null) {
			intent.putExtra(MESSAGE, ponto);
			intent.putExtra(MESSAGE_USUARIO, this.usuario);
		}
		startActivity(intent);
	}
	
}
