package br.com.viseduandroid.view.ponto;


import br.com.viseduandroid.R;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseGlobal;
import br.com.viseduandroid.view.listapontos.ListaPontosGlobaisActivity;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

public class PontoGlobalActivity extends ActionBarActivity {

	private PontoInteresseGlobal ponto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ponto_global);
		
		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(ListaPontosGlobaisActivity.MESSAGE)) {
			ponto = (PontoInteresseGlobal) intent
					.getSerializableExtra(ListaPontosGlobaisActivity.MESSAGE);
		}
		
		if(ponto != null){
			EditText edit = (EditText) findViewById(R.id.editTextNomePontoGlobal);
			edit.setText(ponto.getNome());
			
			edit = (EditText) findViewById(R.id.editTextDescPontoGlobal);
			edit.setText(ponto.getDescricao());
			
			edit = (EditText) findViewById(R.id.editTextLatitudePontoGlobal);
			edit.setText(ponto.getLocalizacao().getLatitude().toString());
			
			edit = (EditText) findViewById(R.id.editTextLongitudePontoGlobal);
			edit.setText(ponto.getLocalizacao().getLongitude().toString());
			
			Spinner spinner = (Spinner) findViewById(R.id.spinnerTipo);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
	                this, R.array.tipo_ponto, android.R.layout.simple_spinner_item);
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        spinner.setAdapter(adapter);
	        spinner.setSelection(ponto.getTipo() -1);
	        spinner.setClickable(false);
	        spinner.setLongClickable(false);
	        spinner.setEnabled(false);
	        
	        ImageView image = (ImageView) findViewById(R.id.imagePontoGlobal);
	        
	        switch (ponto.getTipo()) {
			case 1: //Biblioteca
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 2: //Sala de Aula
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 3: //Auditorio
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 4: //Laboratorio
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 5: //Cantina/Restaurante
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 6: //Banheiro
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			default://Personalizado
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			}
		}
	}

	public void voltar(View view){
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ponto, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}
}
