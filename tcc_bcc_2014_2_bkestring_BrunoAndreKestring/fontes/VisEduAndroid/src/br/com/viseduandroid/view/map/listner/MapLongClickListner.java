/**
 * 
 */
package br.com.viseduandroid.view.map.listner;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.ponto.PontoLocalActivity;

import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.LatLng;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 28/08/2014
 * @version 1.0
 */
public class MapLongClickListner implements OnMapLongClickListener{

	private Context context;
	private Usuario usuario;
	public final static String MESSAGE_PONTOLOCAL = "MapLongClickListner";
	public final static String MESSAGE_PONTOLOCAL_USUARIO = "MapLongClickListnerUsuario";
	
	public MapLongClickListner(Context context, Usuario usuario) {
		this.context = context;
		this.usuario = usuario;
	}
	
	@Override
	public void onMapLongClick(LatLng location) {
		//Toast.makeText(context, "Detectou Longo Click", Toast.LENGTH_SHORT).show();
		double[] latlong = { location.latitude, location.longitude };
		// Fazer com que seja adicionado um novo ponto de interesse do usu�rio
		// direcionando para a tab de pontos de interesse
		Intent intent = new Intent(context, PontoLocalActivity.class);
		intent.putExtra(MESSAGE_PONTOLOCAL, latlong);
		intent.putExtra(MESSAGE_PONTOLOCAL_USUARIO, this.usuario);
		context.startActivity(intent);
	}

}
