/**
 * 
 */
package br.com.viseduandroid.view.map.listner;

import br.com.viseduandroid.view.map.MapaActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 28/08/2014
 * @version 1.0
 */
public class MyLocationButtonListner implements OnMyLocationButtonClickListener{

	private MapaActivity frag;
	
	public MyLocationButtonListner(MapaActivity frag) {
		this.frag = frag;
	}
	
	@Override
	public boolean onMyLocationButtonClick() {
		//Toast.makeText(frag, "Cliquei no Botao de Minha localizacao", Toast.LENGTH_SHORT).show();
		if(frag.getMarkerUsuario() != null){
			frag.getMap().moveCamera(
					CameraUpdateFactory.newLatLngZoom(
							frag.getMarkerUsuario().getPosition(), 16));
		}
		//Retorna true para consumir o comportamento (behave) 
		//e nao fazer o comportamento padrao
		return true;
	}

}
