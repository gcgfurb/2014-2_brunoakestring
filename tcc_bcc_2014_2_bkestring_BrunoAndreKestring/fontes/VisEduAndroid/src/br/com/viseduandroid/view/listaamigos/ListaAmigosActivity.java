package br.com.viseduandroid.view.listaamigos;

import java.util.List;

import br.com.viseduandroid.R;
import br.com.viseduandroid.R.id;
import br.com.viseduandroid.adapter.AmizadeAdapter;
import br.com.viseduandroid.model.amizade.Amizade;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.amigo.AmigoActivity;
import br.com.viseduandroid.view.listapontos.ListaPontosGlobaisActivity;
import br.com.viseduandroid.view.listapontos.ListaPontosLocaisActivity;
import br.com.viseduandroid.view.map.MapaActivity;
import br.com.viseduandroid.webserviceclient.rest.amizade.AmizadeREST;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class ListaAmigosActivity extends ActionBarActivity {

	private Usuario usuario;
	private List<Amizade> listaAmizade;
	private AmizadeAdapter adapter;
	
	public static final String MESSAGE = "ListaAmigosActivity";
	public static final String MESSAGE_AMIGOS = "ListaAmigosActivityAmigos";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_amigos);
		
		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(MapaActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(MapaActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaPontosGlobaisActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaPontosGlobaisActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaPontosLocaisActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaPontosLocaisActivity.MESSAGE);
		}
		
		atualizaLista();
		
		adapter = new AmizadeAdapter(this, listaAmizade);
		
		if(listaAmizade != null){
			ListView listView = (ListView) findViewById(R.id.listaAmigos);
			listView.requestFocus();
			listView.setAdapter(adapter);
		}
	}
	
	protected void onResume(){
		super.onResume();
		atualizaLista();
		adapter.setLista(listaAmizade);
	}
	
	private void atualizaLista(){
		if(usuario != null){
			try {
				listaAmizade = new AmizadeREST().getListaAmizades(usuario.getCodigo());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void adicionarAmigos(View view){
		Intent intent = new Intent(this, AdicionaAmigo.class);
		if (usuario != null) {
			intent.putExtra(MESSAGE, usuario);
		}
		startActivity(intent);
	}
	
	public void editandoAmizade(View view){
		Amizade amizade = (Amizade) view.getTag();
		Intent intent = new Intent(this, AmigoActivity.class);
		if (amizade != null) {
			intent.putExtra(MESSAGE, amizade);
		}
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_amigos, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		Intent intent;
		switch (item.getItemId()) {
		case id.listaPontoGlobal:
			intent = new Intent(this, ListaPontosGlobaisActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		case id.mapa:
			intent = new Intent(this, MapaActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		case id.listaPontoLocal:
			intent = new Intent(this, ListaPontosLocaisActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
