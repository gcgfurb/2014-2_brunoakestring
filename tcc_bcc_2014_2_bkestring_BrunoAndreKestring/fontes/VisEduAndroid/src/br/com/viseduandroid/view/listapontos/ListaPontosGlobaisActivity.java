package br.com.viseduandroid.view.listapontos;

import java.util.List;

import br.com.viseduandroid.R;
import br.com.viseduandroid.R.id;
import br.com.viseduandroid.adapter.PontoGlobalAdapter;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseGlobal;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.listaamigos.ListaAmigosActivity;
import br.com.viseduandroid.view.map.MapaActivity;
import br.com.viseduandroid.view.ponto.PontoGlobalActivity;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseGlobalREST;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class ListaPontosGlobaisActivity extends ActionBarActivity {

	private Usuario usuario;
	private List<PontoInteresseGlobal> listaPontosGlobais;
	
	public static final String MESSAGE = "ListaPontosGlobaisActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_pontos_globais);
		
		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(MapaActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(MapaActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaAmigosActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaAmigosActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaPontosLocaisActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaPontosLocaisActivity.MESSAGE);
		}
		
		if(usuario != null){
			try {
				listaPontosGlobais = new PontoInteresseGlobalREST().getTodosPontosGlobais();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(listaPontosGlobais!= null){
			ListView listView = (ListView) findViewById(R.id.listPontosGlobais);
			listView.requestFocus();
			listView.setAdapter(new PontoGlobalAdapter(this, listaPontosGlobais));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_pontos_globais, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		Intent intent;
		switch (item.getItemId()) {
		case id.listaAmigos:
			intent = new Intent(this, ListaAmigosActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		case id.mapa:
			intent = new Intent(this, MapaActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		case id.listaPontoLocal:
			intent = new Intent(this, ListaPontosLocaisActivity.class);
			if(usuario != null){
				 intent.putExtra(MESSAGE, this.usuario);
			 }
			startActivity(intent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void visualizarPontoGlobal(View view){
		PontoInteresseGlobal ponto = (PontoInteresseGlobal) view.getTag();
		Intent intent = new Intent(this, PontoGlobalActivity.class);
		if (ponto != null) {
			intent.putExtra(MESSAGE, ponto);
		}
		startActivity(intent);
	}
	
}
