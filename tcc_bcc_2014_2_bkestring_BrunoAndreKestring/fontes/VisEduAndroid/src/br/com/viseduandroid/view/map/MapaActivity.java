package br.com.viseduandroid.view.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.viseduandroid.MainActivity;
import br.com.viseduandroid.R;
import br.com.viseduandroid.R.id;
import br.com.viseduandroid.model.amizade.Amizade;
import br.com.viseduandroid.model.localizacao.Localizacao;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseGlobal;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseLocal;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.listaamigos.ListaAmigosActivity;
import br.com.viseduandroid.view.listapontos.ListaPontosGlobaisActivity;
import br.com.viseduandroid.view.listapontos.ListaPontosLocaisActivity;
import br.com.viseduandroid.view.map.listner.MapLongClickListner;
import br.com.viseduandroid.view.map.listner.MarkerClickListner;
import br.com.viseduandroid.view.map.listner.MyLocationButtonListner;
import br.com.viseduandroid.view.map.listner.SpinnerOnItemSelectedListner;
import br.com.viseduandroid.webserviceclient.rest.amizade.AmizadeREST;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseGlobalREST;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseLocalREST;
import br.com.viseduandroid.webserviceclient.rest.usuario.UsuarioREST;
import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MapaActivity extends FragmentActivity implements
		ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	private Usuario usuario;
	private Marker markerUsuario;
	private GoogleMap map;
	
	public final static String MESSAGE = "MapaActivity";
	public final static String MESSAGE_PONTOLOCAL = "MapaActivityPontoLocal";

	private Spinner spinner;
	
	private MyLocationButtonListner myLocationListner;
	private MarkerClickListner markerClickListner;
	private MapLongClickListner mapLongClickListner;
	 

	private LocationClient locationClient;

	private List<Amizade> listaAmigos;
	private Map<Integer, Marker> listaMarkersAmigos;
	private List<PontoInteresseGlobal> listaPontosGlobais;
	private Map<Integer, Marker> listaMarkersGlobais;
	private List<PontoInteresseLocal> listaPontosLocais;
	private Map<Integer, Marker> listaMarkersLocais;

	// These settings are the same as the settings for the map. They will in
	// fact give you updates
	// at the maximal rates currently possible.
	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(5000) // 5 seconds
			.setFastestInterval(16) // 16ms = 60fps
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mapa);

		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(MainActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(MainActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaAmigosActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaAmigosActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaPontosGlobaisActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaPontosGlobaisActivity.MESSAGE);
		}else if(intent != null && intent.hasExtra(ListaPontosLocaisActivity.MESSAGE)){
			usuario = (Usuario) intent
					.getSerializableExtra(ListaPontosGlobaisActivity.MESSAGE);
		}

		myLocationListner = new MyLocationButtonListner(this);
		markerClickListner = new MarkerClickListner(this);
		mapLongClickListner = new MapLongClickListner(this, usuario);
		
		spinner = (Spinner) findViewById(R.id.spinnerMapa);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.camera_position, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new SpinnerOnItemSelectedListner(this, usuario));
        
//        context = getApplicationContext();
//		regid = getRegistrationId(context);
//		
//		if (TextUtils.isEmpty(regid)){
//			registerInBackground();
//	    }else{
//	    	Log.i("TESTE", "J� possui REGID");
//	    }
        
	}

	public Spinner getSpinner(){
		return this.spinner;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.mapa, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Realizar um case pelo �Id� dos itens e logo em seguida mostrar
		// a actividade correspondente
		Intent intent;
		switch (item.getItemId()) {
		case id.listaAmigos:
			intent = new Intent(this, ListaAmigosActivity.class);
			if (usuario != null) {
				intent.putExtra(MESSAGE, this.usuario);
			}
			startActivity(intent);
			break;
		case id.listaPontoGlobal:
			intent = new Intent(this, ListaPontosGlobaisActivity.class);
			if (usuario != null) {
				intent.putExtra(MESSAGE, this.usuario);
			}
			startActivity(intent);
			break;
		case id.listaPontoLocal:
			intent = new Intent(this, ListaPontosLocaisActivity.class);
			if (usuario != null) {
				intent.putExtra(MESSAGE, this.usuario);
			}
			startActivity(intent);
			break;
		}
		// Retornar a classe pai
		return super.onOptionsItemSelected(item);
	}

	public GoogleMap getMap() {
		return this.map;
	}

	public Marker getMarkerUsuario() {
		return this.markerUsuario;
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		locationClient.connect();
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (map == null) {
			// Try to obtain the map from the SupportMapFragment.
			map = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (map != null) {
				setUpMap();
			}
		}else{
			setUpMap();
		}
	}

	private void setUpMap() {
		map.setMyLocationEnabled(true);
		map.setOnMyLocationButtonClickListener(myLocationListner);
		map.setOnMarkerClickListener(markerClickListner);
		map.setOnMapLongClickListener(mapLongClickListner);
		map.setIndoorEnabled(true);
	}
	
	private void atualizarPontos(){
		drawAmigos();
		drawWayPointsGlobais();
		drawWayPointsLocais();
	}
	

	private void setUpLocationClientIfNeeded() {
		if (locationClient == null) {
			locationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (locationClient != null) {
			locationClient.disconnect();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// � chamado a cada 5 segundos de acordo com a configura�ao do
		// LocationRequest
		// aqui ser� capturada a posicao atual do usuario
		Localizacao localizacao = new 
				Localizacao(location.getLatitude(),
				location.getLongitude());
		usuario.setLocalizacao(localizacao);
		LatLng latLng = new LatLng(location.getLatitude(),
				location.getLongitude());
		if (markerUsuario == null) {
			markerUsuario = map.addMarker(new MarkerOptions()
			.position(latLng)
			.title(usuario.getNome())
			.snippet("Estou aqui!"));
			//.icon(BitmapDescriptorFactory.fromResource(R.drawable.livros)));
		} else {
			markerUsuario.setPosition(latLng);
		}
//		new AsyncTask<Usuario, Void, Void>() {
//
//			@Override
//			protected Void doInBackground(Usuario... params) {
//				long time1 = System.currentTimeMillis();
				new UsuarioREST().updatePosicao(this.usuario);
				atualizarPontos();
//				long time2 = System.currentTimeMillis();
//				Log.i("TESTANDO", "Tempo de execu��o: " + (time2 - time1));
//				return null;
//			}
//		}.execute(this.usuario);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {

	}

	@Override
	public void onConnected(Bundle connectionHint) {
		locationClient.requestLocationUpdates(REQUEST, this); // LocationListener
	}

	@Override
	public void onDisconnected() {
		locationClient.disconnect();
	}

	private void drawAmigos() {
		carregaListaAmigos();
		if (listaAmigos != null && !listaAmigos.isEmpty()) {
			for (Amizade a : listaAmigos) {
				Marker marker = listaMarkersAmigos
						.get(a.getAmigo().getCodigo());
				if (marker != null) {
					LatLng latlng = new LatLng(a.getAmigo().getLocalizacao()
							.getLatitude(), a.getAmigo().getLocalizacao()
							.getLongitude());
					marker.setPosition(latlng);
				} else {
					marker = map.addMarker(new MarkerOptions()
							.position(
									new LatLng(a.getAmigo().getLocalizacao()
											.getLatitude(), a.getAmigo()
											.getLocalizacao().getLongitude()))
							.title(a.getAmigo().getNome())
							.snippet(a.getAmigo().getEmail())
							.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
							);
					listaMarkersAmigos.put(a.getAmigo().getCodigo(), marker);
				}
			}
		}
	}

	private void carregaListaAmigos() {
		try {
			if(listaMarkersAmigos == null){
				listaMarkersAmigos = new HashMap<Integer, Marker>();
			}
			AmizadeREST rest = new AmizadeREST();
			this.listaAmigos = rest.getAmigosLogados(this.usuario
					.getCodigo());
		} catch (Exception e) {
			String erro = e.getMessage();
			e.printStackTrace();
			Log.i("ERRO", "Ocorreu erro ao recuperar a lista de Amigos");
		}
	}

	private void drawWayPointsGlobais() {
		carregaListaPontosGlobais();
		if (listaPontosGlobais != null && !listaPontosGlobais.isEmpty()) {
			for (PontoInteresseGlobal p : listaPontosGlobais) {
				Marker marker = listaMarkersGlobais.get(p.getCodigo());
				if (marker == null) {
					MarkerOptions options = new MarkerOptions();
					options.position(
							new LatLng(p.getLocalizacao().getLatitude(),
									p.getLocalizacao().getLongitude()))
						   .title(p.getNome())
						   .snippet(p.getDescricao())
						   .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
					marker = map.addMarker(options);
					listaMarkersGlobais.put(p.getCodigo(), marker);
				}
			}
		}
	}

	private void carregaListaPontosGlobais() {
		try {
			if(listaMarkersGlobais == null){
				listaMarkersGlobais = new HashMap<Integer, Marker>();
			}
			this.listaPontosGlobais = new PontoInteresseGlobalREST()
					.getTodosPontosGlobais();
		} catch (Exception e) {
			e.printStackTrace();
			Log.i("ERRO", "Ocorreu erro ao recuperar a lista de Pontos Globais");
		}
	}
	
	private void drawWayPointsLocais(){
		carregarListaPontosLocais();
		if(listaPontosLocais != null && !listaPontosLocais.isEmpty()){
			for (PontoInteresseGlobal p : listaPontosLocais) {
				Marker marker = listaMarkersLocais.get(p.getCodigo());
				if (marker == null) {
					marker = map.addMarker(new MarkerOptions()
					.position(
							new LatLng(p.getLocalizacao().getLatitude(),
									   p.getLocalizacao().getLongitude()))
					.title(p.getNome())
					.snippet(p.getDescricao())
					.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
					listaMarkersLocais.put(p.getCodigo(), marker);
				}
			}
		}
	}
	
	private void carregarListaPontosLocais(){
		try{
			if(listaMarkersLocais == null){
				listaMarkersLocais = new HashMap<Integer, Marker>();
			}
			this.listaPontosLocais = new PontoInteresseLocalREST().
					getTodosPontosLocais(this.usuario.getCodigo());
		}catch(Exception e){
			//e.printStackTrace();
			Log.i("ERRO", "Ocorreu erro ao recuperar a lista de Pontos Locais");
		}
	}
	
	private void setIconMarker(PontoInteresseGlobal ponto, Marker marker){
		switch (ponto.getTipo()) {
		case 1: //Biblioteca
			marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stat_cloud2));
			break;
		case 2: //Sala de Aula
			
			break;
		case 3: //Auditorio
			
			break;
		case 4: //Laboratorio
			
			break;
		case 5: //Cantina/Restaurante
			
			break;
		case 6: //Banheiro
			
			break;
		default://Personalizado
			
			break;
		}
	}
	
}