/**
 * 
 */
package br.com.viseduandroid.view.map.listner;

import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.Toast;
import br.com.viseduandroid.view.map.MapaActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.Marker;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 28/08/2014
 * @version 1.0
 */
public class MarkerClickListner implements OnMarkerClickListener {

	private MapaActivity mapa;
	
	public MarkerClickListner(MapaActivity mapa) {
		this.mapa = mapa;
	}
	
	@Override
	public boolean onMarkerClick(final Marker marker) {
		//Toast.makeText(frag, "Detectou Click Marker", Toast.LENGTH_SHORT).show();
		//Retorna true para identificar que ele consome o evento de click no marker
		//gerado para que o comportamento padrao nao seja realizado
		// This causes the marker at Perth to bounce into position when it is clicked.
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500;

        final Interpolator interpolator = new BounceInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(1 - interpolator
                        .getInterpolation((float) elapsed / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 2 * t);

                if (t > 0.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
		return false;
	}

}
