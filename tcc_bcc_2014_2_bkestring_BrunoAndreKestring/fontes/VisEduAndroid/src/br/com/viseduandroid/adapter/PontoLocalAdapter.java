/**
 * 
 */
package br.com.viseduandroid.adapter;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.viseduandroid.R;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseGlobal;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseLocal;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseLocalREST;

/**
 * Classe responsavel por
 * 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 30/09/2014
 * @version 1.0
 */
public class PontoLocalAdapter extends BaseAdapter {

	private Context context;
	private List<PontoInteresseLocal> lista;

	public static final String MESSAGE = "PontoLocalAdapter";

	public PontoLocalAdapter(Context context, List<PontoInteresseLocal> lista) {
		this.context = context;
		this.lista = lista;
	}

	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int index) {
		return this.lista.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public boolean excluir(PontoInteresseLocal ponto) {
		if (lista.contains(ponto)) {
			return lista.remove(ponto);
		}
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final PontoInteresseLocal ponto = lista.get(position);
		final Context contexto = this.context;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.pontos_locais, null);
			convertView.setTag(this.lista.get(position));
		}
		TextView nome = (TextView) convertView
				.findViewById(R.id.textNomePontoLocal);
		nome.setText(ponto.getNome());

		ImageView image = (ImageView) convertView
				.findViewById(R.id.imagePontoLocal);
		image.setImageResource(R.drawable.ic_stat_cloud2);

		Button botaoExcluir = (Button) convertView
				.findViewById(R.id.botaoExcluirPonto);
		botaoExcluir.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					if (ponto != null) {
						Log.d("TESTANDO", " ");
						new PontoInteresseLocalREST()
								.excluir(ponto);
						excluir(ponto);
						notifyDataSetChanged();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(contexto, e.getMessage(),
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		return convertView;
	}
}
