/**
 * 
 */
package br.com.viseduandroid.adapter;

import java.util.List;

import br.com.viseduandroid.R;
import br.com.viseduandroid.model.amizade.Amizade;
import br.com.viseduandroid.webserviceclient.rest.amizade.AmizadeREST;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseLocalREST;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Classe responsavel por
 * 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 26/09/2014
 * @version 1.0
 */
public class AmizadeAdapter extends BaseAdapter {

	private Context context;
	private List<Amizade> lista;
	
	public static final String MESSAGE = "AmizadeAdapter";

	public AmizadeAdapter(Context context, List<Amizade> lista) {
		this.context = context;
		this.lista = lista;
	}

	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int index) {
		return this.lista.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public boolean excluir(Amizade amizade){
		if (lista.contains(amizade)) {
			return lista.remove(amizade);
		}
		return false;
	}
	
	public void setLista(List<Amizade> lista){
		this.lista = lista;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Amizade amizade = lista.get(position);
		
		View layout;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.amigos, null);
			convertView.setTag(amizade);
		}
		
		TextView nome = (TextView) convertView.findViewById(R.id.textNome);
		nome.setText(amizade.getAmigo().getNome());
		TextView email = (TextView) convertView.findViewById(R.id.textEmail);
		email.setText(amizade.getAmigo().getEmail());

		ImageView image = (ImageView) convertView.findViewById(R.id.image);
		image.setImageResource(R.drawable.ic_stat_cloud2);

		Button botaoExcluir = (Button) convertView
				.findViewById(R.id.botaoExcluirAmizade);
		botaoExcluir.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					if (amizade != null) {
						new AmizadeREST()
								.deletarAmizade(amizade);
						excluir(amizade);
						notifyDataSetChanged();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(context, e.getMessage(),
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		return convertView;
	}

}
