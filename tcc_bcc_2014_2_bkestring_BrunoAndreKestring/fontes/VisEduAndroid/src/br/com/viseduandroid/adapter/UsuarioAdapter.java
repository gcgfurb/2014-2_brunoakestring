/**
 * 
 */
package br.com.viseduandroid.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.com.viseduandroid.R;
import br.com.viseduandroid.model.amizade.Amizade;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.webserviceclient.rest.amizade.AmizadeREST;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 07/11/2014
 * @version 1.0
 */
public class UsuarioAdapter extends BaseAdapter {

	private Context context;
	private List<Usuario> lista;
	private Usuario usuario;

	public UsuarioAdapter(Context context,Usuario usuario,
			List<Usuario> lista){
		this.context = context;
		this.usuario = usuario;
		this.lista = lista;
	}
	
	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int index) {
		return this.lista.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public boolean excluir(Usuario usuario){
		if (lista.contains(usuario)) {
			return lista.remove(usuario);
		}
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Usuario amigo = lista.get(position);
		
		View layout;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = inflater.inflate(R.layout.usuario_adapter, null);
		}else{
			layout = convertView;
		}
		
		Button btn = (Button) layout.findViewById(R.id.btnAdicionaAmigo);
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Amizade amizade = new Amizade();
				amizade.setUsuario(usuario);
				amizade.setAmigo(amigo);
				try {
					new AmizadeREST().inserirAmizade(amizade);
					excluir(amigo);
					notifyDataSetChanged();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		TextView nome = (TextView) layout.findViewById(R.id.textUsuarioNome);
		nome.setText(amigo.getNome());

		return layout;
	}
}
