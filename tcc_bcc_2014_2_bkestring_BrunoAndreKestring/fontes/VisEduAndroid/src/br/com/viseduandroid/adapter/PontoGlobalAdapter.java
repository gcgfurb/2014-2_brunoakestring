/**
 * 
 */
package br.com.viseduandroid.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.viseduandroid.R;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseGlobal;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 26/09/2014
 * @version 1.0
 */
public class PontoGlobalAdapter extends BaseAdapter {

	private Context context;
	private List<PontoInteresseGlobal> lista;
	
	public static final String MESSAGE = "PontoGlobalAdapter";

	public PontoGlobalAdapter(Context context, List<PontoInteresseGlobal> lista) {
		this.context = context;
		this.lista = lista;
	}

	@Override
	public int getCount() {
		return this.lista.size();
	}

	@Override
	public Object getItem(int index) {
		return this.lista.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PontoInteresseGlobal ponto = lista.get(position);
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.pontos_globais, null);
			convertView.setTag(ponto);
		}
		//TODO
		TextView nome = (TextView) convertView.findViewById(R.id.textNomePontoGlobal);
		nome.setText(ponto.getNome());

		ImageView image = (ImageView) convertView.findViewById(R.id.imagePontoGlobal);
		image.setImageResource(R.drawable.ic_stat_cloud2);

		return convertView;
	}

}
