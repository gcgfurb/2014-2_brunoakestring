package br.com.viseduandroid.webserviceclient.rest.amizade;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.Toast;
import br.com.viseduandroid.model.amizade.Amizade;
import br.com.viseduandroid.webserviceclient.cliente.WebserviceCliente;

/**
 * Created by Bruno on 18/08/2014.
 */
public class AmizadeREST {

    //private static final String URL_WS = "http://192.168.16.1:8080/WebServiceVisEdu/amizade/";
    //private static final String URL_WS = "http://192.168.56.1:8080/WebServiceVisEdu/amizade/";
    //private static final String URL_WS = "http://187.85.122.85:8080/WebServiceVisEdu/amizade/";
    //private static final String URL_WS = "http://192.168.0.13:8080/WebServiceVisEdu/amizade/";
    private static final String URL_WS = "http://192.168.169.5:8080/WebServiceVisEdu/amizade/";
    //private static final String URL_WS =  "http://campeche.inf.furb.br:8094/FURB-Mobile/WebServiceVisEdu/amizade/";
    
    public Amizade getAmizade(int codUsuario, int codAmigo) throws Exception {
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscar/" + codUsuario + "/" + codAmigo);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            Amizade amizade = gson.fromJson(resposta[1],Amizade.class);
            return amizade;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public List<Amizade> getListaAmizades(int codUsuario) throws Exception {
        List<Amizade> amizades;
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscar/" + codUsuario);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            amizades = new ArrayList<Amizade>();
            JsonParser jsonParser = new JsonParser();
            JsonArray array = jsonParser.parse(resposta[1]).getAsJsonArray();
            for (int i = 0; i < array.size(); i++){
                amizades.add(gson.fromJson(array.get(i), Amizade.class));
            }
            return amizades;
        }else{
            throw new Exception(resposta[1]);
        }
    }
    
    public List<Amizade> getAmigosLogados(int codUsuario) throws Exception {
        List<Amizade> amizades;
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscarAmigosLogados/" + codUsuario);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            amizades = new ArrayList<Amizade>();
            JsonParser jsonParser = new JsonParser();
            JsonArray array = jsonParser.parse(resposta[1]).getAsJsonArray();
            for (int i = 0; i < array.size(); i++){
                amizades.add(gson.fromJson(array.get(i), Amizade.class));
            }
            return amizades;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public List<Amizade> getTodos() throws Exception {
        List<Amizade> amizades;
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscarTodosGSON");
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            amizades = new ArrayList<Amizade>();
            JsonParser jsonParser = new JsonParser();
            JsonArray array = jsonParser.parse(resposta[1]).getAsJsonArray();
            for (int i = 0; i < array.size(); i++){
                amizades.add(gson.fromJson(array.get(i), Amizade.class));
            }
            return amizades;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public String inserirAmizade(Amizade amizade) throws Exception {
        Gson gson = new Gson();
        String amizadeJson = gson.toJson(amizade);
        String resposta[] = new WebserviceCliente().post(URL_WS + "inserir", amizadeJson);
        if(resposta[0].equals("200")){
            return resposta[1];
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public String deletarAmizade(Amizade amizade){
    	Gson gson = new Gson();
        String amizadeJson = gson.toJson(amizade);
        String[] resposta = new WebserviceCliente().post(URL_WS + "deletar" ,amizadeJson);
        return resposta[1];
    }
    
    public String atualizaPermissao(Amizade amizade){
    	Gson gson = new Gson();
    	String amizadeJson = gson.toJson(amizade);
    	String[] resposta = new WebserviceCliente().post(URL_WS + "atualizaPermissao", amizadeJson);
    	return resposta[1];
    }

}
