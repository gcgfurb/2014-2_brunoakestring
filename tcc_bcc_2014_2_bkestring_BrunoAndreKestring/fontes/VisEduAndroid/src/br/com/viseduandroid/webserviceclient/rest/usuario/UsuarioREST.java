package br.com.viseduandroid.webserviceclient.rest.usuario;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.webserviceclient.cliente.WebserviceCliente;

/**
 * Created by Bruno on 18/08/2014.
 */
public class UsuarioREST {

    //private static final String URL_WS = "http://192.168.16.1:8080/WebServiceVisEdu/usuario/";
    //private static final String URL_WS = "http://192.168.56.1:8080/WebServiceVisEdu/usuario/";
    //private static final String URL_WS = "http://192.168.0.13:8080/WebServiceVisEdu/usuario/";
    //private static final String URL_WS = "http://187.85.122.85:8080/WebServiceVisEdu/usuario/";
    private static final String URL_WS = "http://192.168.169.5:8080/WebServiceVisEdu/usuario/";
	//private static final String URL_WS =  "http://campeche.inf.furb.br:8094/FURB-Mobile/WebServiceVisEdu/usuario/";
	
    public Usuario getUsuario(int id) throws Exception {
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscar/" + id);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            Usuario usuario = gson.fromJson(resposta[1],Usuario.class);
            return usuario;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public Usuario getUsuarioByNome(String nome) throws Exception{
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscarByNome/" + nome);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            Usuario usuario = gson.fromJson(resposta[1],Usuario.class);
            return usuario;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public List<Usuario> getTodos() throws Exception {
        List<Usuario> usuarios;
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscarTodosGSON");
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            usuarios = new ArrayList<Usuario>();
            JsonParser jsonParser = new JsonParser();
            JsonArray array = jsonParser.parse(resposta[1]).getAsJsonArray();
            for (int i = 0; i < array.size(); i++){
                usuarios.add(gson.fromJson(array.get(i), Usuario.class));
            }
            return usuarios;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public String inserirUsuario(Usuario usuario) throws Exception {
        Gson gson = new Gson();
        String usuarioJson = gson.toJson(usuario);
        String resposta[] = new WebserviceCliente().post(URL_WS + "inserir", usuarioJson);
        if(resposta[0].equals("200")){
            return resposta[1];
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public String deletarUsuario(Usuario usuario){
        String[] resposta = new WebserviceCliente().get(URL_WS + "deletar/" + usuario.getCodigo());
        return resposta[1];
    }

    public boolean validarUsuario(String nome, String email) throws Exception{
    	String ret[] = new WebserviceCliente().get(URL_WS + "validarUsuario/" + nome + "/" + email);
        if(ret[0].equals("200")){
        	return true;
        }else{
        	throw new Exception("Erro ao validar usuario: " + ret[1]);
        	
        }
    }

    public Usuario validarSenha(String nome, String senha) throws Exception{
        String ret[] = new WebserviceCliente().get(URL_WS + "validarSenha/" + nome + "/" + senha);
        if(ret[0].equals("200")){
        	Gson gson = new Gson();
            Usuario usuario = gson.fromJson(ret[1],Usuario.class);
            return usuario;
        }else{
        	if("0".equals(ret[0])){
        		throw new Exception("Servi�o Web est� fora!");
        	}
        	throw new Exception(ret[1]);
        }
    }
    
    public void updatePosicao(Usuario usuario){
    	int codUsuario = usuario.getCodigo();
    	double latitude = usuario.getLocalizacao().getLatitude();
    	double longitude = usuario.getLocalizacao().getLongitude();
    	String ret[] = new WebserviceCliente().get(URL_WS + "updatePosicao/" + codUsuario + "/" + latitude + "/" + longitude);
        if(ret[0].equals("200")){
        	Log.i("POSICAO", "Posicao atualizada com sucesso!");
        }else{
        	Log.i("POSICAO", "Posicao n�o foi atualizada");
        }
    }
    
    public List<Usuario> getUsuarioNaoAmigo(int codUsuario) throws Exception{
    	String ret[] = new WebserviceCliente().get(URL_WS + "usuarioNaoAmigo/" + codUsuario);
    	if("200".equals(ret[0])){
    		Gson gson = new Gson();
            List<Usuario> usuarios = new ArrayList<Usuario>();
            JsonParser jsonParser = new JsonParser();
            JsonArray array = jsonParser.parse(ret[1]).getAsJsonArray();
            for (int i = 0; i < array.size(); i++){
                usuarios.add(gson.fromJson(array.get(i), Usuario.class));
            }
            return usuarios;
        }else{
            throw new Exception(ret[1]);
        }
    }
    
    public Usuario registraUsuario(String nome, String senha) throws Exception{
        String ret[] = new WebserviceCliente().get(URL_WS + "registra/" + nome + "/" + senha);
        if(ret[0].equals("200")){
        	Gson gson = new Gson();
            Usuario usuario = gson.fromJson(ret[1],Usuario.class);
            return usuario;
        }else{
        	if("0".equals(ret[0])){
        		throw new Exception("Servi�o Web est� fora!");
        	}
        	throw new Exception(ret[1]);
        }
    }
}
