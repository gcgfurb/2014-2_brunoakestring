/**
 * 
 */
package br.com.visedu.model.ccsmodel;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe responsavel por
 * 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 15/10/2014
 * @version 1.0
 */
@XmlRootElement
public class NotificationKey {

	// @DatabaseField(id = true)
	private String name;
	// @DatabaseField
	private String notificacitonKey;

	public NotificationKey() {

	}

	public NotificationKey(String name, String notificacitonKey) {
		this.notificacitonKey = notificacitonKey;
		this.name = name;
	}

	public String getNotificacitonKey() {
		return notificacitonKey;
	}

	public void setNotificacitonKey(String notificacitonKey) {
		this.notificacitonKey = notificacitonKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name + " : " + notificacitonKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result	+ ((notificacitonKey == null) ? 0 : notificacitonKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificationKey other = (NotificationKey) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (notificacitonKey == null) {
			if (other.notificacitonKey != null)
				return false;
		} else if (!notificacitonKey.equals(other.notificacitonKey))
			return false;
		return true;
	}

}
