package br.com.visedu.model.localizacao;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe respons�vel por guardar as informa��es de localiza��o dos objetos no mundo
 * @author Bruno Andr� Kestring - kestringbak@gmail.com - 10/08/2014 15:05 
 */
@XmlRootElement
public class Localizacao implements Serializable{

	private Double latitude;
	private Double longitude;
	
	public Localizacao(){
		
	}
	
	public Localizacao(Double latitude, Double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result
				+ ((longitude == null) ? 0 : longitude.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Localizacao other = (Localizacao) obj;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		return true;
	}
	
}
