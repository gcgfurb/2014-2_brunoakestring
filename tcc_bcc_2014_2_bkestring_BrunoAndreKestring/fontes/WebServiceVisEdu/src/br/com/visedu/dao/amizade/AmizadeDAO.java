package br.com.visedu.dao.amizade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.visedu.dao.IDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.dao.usuario.UsuarioDAO;
import br.com.visedu.factory.connection.ConnectionManager;
import br.com.visedu.model.amizade.Amizade;
import br.com.visedu.model.usuario.Usuario;

/**
 * Classe responsavel por Implementar a logica DAO da classe Amizade
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 10/08/2014
 * @version 1.0
 */
public class AmizadeDAO implements IDAO<Amizade> {

	private static AmizadeDAO instance;
	
	private AmizadeDAO() {
	}
	
	/**
	 * Metodo responsavel por controlar o acesso a instancia da classe AmizadeDAO
	 * @return the instance
	 */
	public static AmizadeDAO getInstance() {
		if(instance == null){
			instance = new AmizadeDAO();
		}
		return instance;
	}
	
	@Override
	public void inserir(Amizade t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "INSERT INTO amizade("
				+ " cod_usuario,"
				+ " cod_amigo,"
				+ " flg_permissao)"
				+ " VALUES (?, ?, ?)";

		stm = conn.prepareStatement(sql);
		
		stm.setInt(1, t.getUsuario().getCodigo());
		stm.setInt(2, t.getAmigo().getCodigo());
		stm.setString(3, t.isPermicao() ? "S" : "N");
		
		stm.execute();
	}

	@Override
	public void atualizar(Amizade t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "UPDATE amizade"
					 + " SET cod_amigo = ?,"
					 + " flg_permissao = ?"
				   + " WHERE cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t.getAmigo().getCodigo());
		stm.setString(2, t.isPermicao() ? "S" : "N");
		stm.setInt(3, t.getUsuario().getCodigo());

		stm.execute();

	}

	@Override
	public void remover(Amizade t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM amizade"
				   + " WHERE cod_usuario = ? "
				   + "   AND cod_amigo = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t.getUsuario().getCodigo());
		stm.setInt(2, t.getAmigo().getCodigo());

		stm.execute();
	}
	
	public void removerTodos(Amizade t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM amizade"
				   + " WHERE cod_usuario = ? ";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t.getUsuario().getCodigo());

		stm.execute();
	}

	public Amizade retornarAmizade(Integer codUsuario, Integer codAmigo) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_usuario, "
					+ " cod_amigo, "
					+ " flg_permissao"
				    + " FROM amizade "
				   + " WHERE cod_usuario = ?"
				   + "   AND cod_amigo = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, codUsuario);
		stm.setInt(2, codAmigo);

		rs = stm.executeQuery();
		
		Amizade a = null;
		while(rs.next()){
			a = new Amizade();
			Integer codU = rs.getInt("cod_usuario");
			Usuario u = UsuarioDAO.getInstance().retornaUsuario(codU);
			a.setUsuario(u);
			Integer codA = rs.getInt("cod_amigo");
			u = UsuarioDAO.getInstance().retornaUsuario(codA);
			a.setUsuario(u);
			String s = rs.getString("flg_permissao");
			a.setPermicao( s.equalsIgnoreCase("S") ? true : false);
		}
		
		if(a == null){
			throw new DAOException("Amizade n�o encontrada. Cod : " + codUsuario + " Amig:" + codAmigo, 1001);
		}
		return a;
	}
	
	/**
	 * Metodo responv�l por
	 * @return List<Amizade>
	 * @throws DAOException
	 * @throws SQLException
	 */
	public List<Amizade> retornarListaAmizades(Integer codUsuario) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		List<Amizade> amizades = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		amizades = new ArrayList<Amizade>();
		
		String sql = "select cod_usuario, "
					+ " cod_amigo, "
					+ " flg_permissao"
				    + " FROM amizade "
				   + " WHERE cod_usuario = ? ";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, codUsuario);

		rs = stm.executeQuery();
		
		Amizade a = null;
		while(rs.next()){
			a = new Amizade();
			Integer codU = rs.getInt("cod_usuario");
			Usuario u = UsuarioDAO.getInstance().retornaUsuario(codU);
			a.setUsuario(u);
			Integer codA = rs.getInt("cod_amigo");
			u = UsuarioDAO.getInstance().retornaUsuario(codA);
			a.setAmigo(u);
			String s = rs.getString("flg_permissao");
			a.setPermicao( s.equalsIgnoreCase("S") ? true : false);
			amizades.add(a);
		}
		
		if(amizades.isEmpty()){
			throw new DAOException("Amizades n�o encontradas. Cod : " + codUsuario, 1002);
		}
		
		return amizades;
	}

	public List<Amizade> retornarAmigosLogados(Integer codUsuario) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		List<Amizade> amizades = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		amizades = new ArrayList<Amizade>();
		
		String sql = "select a.cod_usuario, "
					+ " a.cod_amigo, "
					+ " a.flg_permissao"
				    + " FROM amizade a, usuario u "
				   + " WHERE a.cod_usuario = u.cod_usuario "
				   + "   AND coalesce(a.flg_permissao,'S') = 'S' "
				   + "   AND a.cod_usuario = ? ";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, codUsuario);
		//stm.setString(2, "S");

		rs = stm.executeQuery();
		
		Amizade a = null;
		while(rs.next()){
			a = new Amizade();
			Integer codU = rs.getInt("cod_usuario");
			Usuario u = UsuarioDAO.getInstance().retornaUsuario(codU);
			a.setUsuario(u);
			Integer codA = rs.getInt("cod_amigo");
			u = UsuarioDAO.getInstance().retornaUsuario(codA);
			a.setAmigo(u);
			String s = rs.getString("flg_permissao");
			a.setPermicao( s.equalsIgnoreCase("S") ? true : false);
			amizades.add(a);
		}
		
		if(amizades.isEmpty()){
			throw new DAOException("Amizades n�o encontradas. Cod : " + codUsuario, 1003);
		}
		return amizades;
	}
	
	@Override
	public List<Amizade> retornaTodos() throws DAOException, SQLException {
		List<Amizade> usuarios = null;
		Connection conn = null; 
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		usuarios = new ArrayList<Amizade>();
		conn = ConnectionManager.getInstance().getConexao();
		
		String sql = "select cod_usuario, "
					+ " cod_amigo, "
					+ " flg_permissao"
				    + " FROM amizade";
		
		stm = conn.prepareStatement(sql);
		
		rs = stm.executeQuery();
		
		Amizade amizade = null;
		while(rs.next()){
			amizade = new Amizade();
			
			amizade = new Amizade();
			Integer codU = rs.getInt("cod_usuario");
			Usuario u = UsuarioDAO.getInstance().retornaUsuario(codU);
			amizade.setUsuario(u);
			Integer codA = rs.getInt("cod_amigo");
			u = UsuarioDAO.getInstance().retornaUsuario(codA);
			amizade.setUsuario(u);
			String s = rs.getString("flg_permissao");
			amizade.setPermicao( s.equalsIgnoreCase("S") ? true : false);
			
			usuarios.add(amizade);
		}
		
		if(usuarios.isEmpty()){
			throw new DAOException("N�o foi poss�vel encontrar todos as Amizades.", 1004);
		}
		
		return usuarios;
	}

	public void atualizaPermissao(Amizade amizade) throws SQLException{
		Connection conn = null; 
		PreparedStatement stm = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		
		String sql = "update amizade "
				   + "   set flg_permissao = ? "
				   + " where cod_usuario = ? "
				   + "   and cod_amigo = ? ";
		
		stm = conn.prepareStatement(sql);
		
		String val = amizade.isPermicao() == true ? "S" : "N"; 
		stm.setString(1, val);
		stm.setInt(2, amizade.getUsuario().getCodigo());
		stm.setInt(3, amizade.getAmigo().getCodigo());
		
		stm.execute();
	}
}
