package br.com.visedu.dao;

import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.exception.DAOException;

public interface IDAO <T>{
    
    public abstract void inserir(T t) throws DAOException,SQLException;
    public abstract void atualizar(T t) throws DAOException,SQLException;
    public abstract void remover(T t) throws DAOException,SQLException;
    public abstract List<T> retornaTodos() throws DAOException,SQLException;

}
