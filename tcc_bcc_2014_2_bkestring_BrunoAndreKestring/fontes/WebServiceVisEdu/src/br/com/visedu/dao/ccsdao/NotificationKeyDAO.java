/**
 * 
 */
package br.com.visedu.dao.ccsdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.IDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.factory.connection.ConnectionManager;
import br.com.visedu.model.ccsmodel.NotificationKey;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 15/10/2014
 * @version 1.0
 */
public class NotificationKeyDAO implements IDAO<NotificationKey>{

	private static NotificationKeyDAO instance;

	private NotificationKeyDAO() {
	}

	/**
	 * Metodo responsavel por controlar o acesso a instancia da classe
	 *  
	 * @return the instance
	 */
	public static NotificationKeyDAO getInstance() {
		if (instance == null) {
			instance = new NotificationKeyDAO();
		}
		return instance;
	}
	
	
	@Override
	public void inserir(NotificationKey t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = 
				"INSERT INTO notification_key"
				+ " key_notification," 
				+ " nom_notification)"
				+ " VALUES (?, ?)";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getName());
		stm.setString(2, t.getNotificacitonKey());

		stm.execute();
	}

	@Override
	public void atualizar(NotificationKey t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "UPDATE notification_key "
					 + " SET key_notification = ?,"
					 + "     nom_notification = ? "
				   + " WHERE key_notification = ?";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getNotificacitonKey());
		stm.setString(2, t.getName());
		stm.setString(3, t.getNotificacitonKey());

		stm.execute();
	}

	@Override
	public void remover(NotificationKey t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM notification_key "
				   + " WHERE key_notification = ? ";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getNotificacitonKey());

		stm.execute();
	}
	
	public void removeByNome(String nome) throws SQLException{
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM notification_key "
				   + " WHERE nom_notification = ? ";

		stm = conn.prepareStatement(sql);

		stm.setString(1, nome);

		stm.execute();
	}
	

	@Override
	public List<NotificationKey> retornaTodos() throws DAOException,
			SQLException {
		return null;
	}
	
	public NotificationKey retornaByKey(String notificationKey) {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select key_notification, "
						 + " nom_notification  "
				    + " FROM notification_key "
				   + " WHERE key_notification = ?";

		try{
			stm = conn.prepareStatement(sql);
	
			stm.setString(1, notificationKey);
	
			rs = stm.executeQuery();
			
			NotificationKey n = null;
			while(rs.next()){
				n = new NotificationKey();
				String key = rs.getString("key_notification");
				String nomNot = rs.getString("nom_notification");
				n.setNotificacitonKey(key);
				n.setName(nomNot);
			}
			return n;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Ocorreu erro ao buscar Notification\n" + e.getMessage());
			return null;
		}
	}
	
	public NotificationKey retornaByNome(String nome) throws SQLException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select key_notification, "
						 + " nom_notification "
				    + " FROM notification_key "
				   + " WHERE nom_notification = ?";

		stm = conn.prepareStatement(sql);

		stm.setString(1, nome);

		rs = stm.executeQuery();
		
		NotificationKey n = null;
		while(rs.next()){
			n = new NotificationKey();
			String key = rs.getString("key_notification");
			String nomNot = rs.getString("nom_notification");
			n.setNotificacitonKey(key);
			n.setName(nomNot);
		}
		return n;
	}

}
