/**
 * 
 */
package br.com.visedu.dao.pontointeresse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.visedu.dao.IDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.dao.usuario.UsuarioDAO;
import br.com.visedu.factory.connection.ConnectionManager;
import br.com.visedu.model.localizacao.Localizacao;
import br.com.visedu.model.pontoInteresse.PontoInteresseLocal;
import br.com.visedu.model.usuario.Usuario;

/**
 * Classe responsavel por Implementar a logica DAO da classe PontoInteresseLocal
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 11/08/2014
 * @version 1.0
 */
public class PontoInteresseLocalDAO implements IDAO<PontoInteresseLocal>{

	private static PontoInteresseLocalDAO instance;
	
	private PontoInteresseLocalDAO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Metodo responsavel por controlar o acesso a instancia da classe PontoInteresseLocalDAO
	 * @return the instance
	 */
	public static PontoInteresseLocalDAO getInstance() {
		if(instance == null){
			instance = new PontoInteresseLocalDAO();
		}
		return instance;
	}
	
	@Override
	public void inserir(PontoInteresseLocal t) throws DAOException,
			SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		if(t.getCodigo() == null){
			String sql = "INSERT INTO ponto_interesse_local("
					+ " cod_usuario,"
					+ " num_ponto_local,"
					+ " nom_ponto_local,"
					+ " dsc_ponto_local,"
					+ " val_latitude,"
					+ " val_longitude,"
					+ " ind_tipo_ponto)"
					+ " VALUES (?, ?, ?, ?, ?, ?,?)";
			
			stm = conn.prepareStatement(sql);
			
			//atualizar o codigo do objeto com o metodo getMaxCod
			t.setCodigo(this.getMaxCod(t.getUsuario().getCodigo()));
			
			stm.setInt(1, t.getUsuario().getCodigo());
			stm.setInt(2, t.getCodigo());
			stm.setString(3, t.getNome());
			stm.setString(4, t.getDescricao());
			stm.setDouble(5, t.getLocalizacao().getLatitude());
			stm.setDouble(6, t.getLocalizacao().getLongitude());
			stm.setInt(7, t.getTipo());
			
			stm.execute();
		}else{
			atualizar(t);
		}
	}

	@Override
	public void atualizar(PontoInteresseLocal t) throws DAOException, SQLException{
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		if(t.getCodigo() != null){
			String sql = "UPDATE ponto_interesse_local"
					+ " SET nom_ponto_local = ?,"
					+ " dsc_ponto_local = ?,"
					+ " val_latitude = ?,"
					+ " val_longitude = ?"
					+ " WHERE cod_usuario = ?"
					+ "   AND num_ponto_local = ?";
			
			stm = conn.prepareStatement(sql);
			
			stm.setString(1, t.getNome());
			stm.setString(2, t.getDescricao());
			stm.setDouble(3, t.getLocalizacao().getLatitude());
			stm.setDouble(4, t.getLocalizacao().getLongitude());
			stm.setInt(5, t.getUsuario().getCodigo());
			stm.setInt(6, t.getCodigo());
			
			stm.execute();
		}else{
			inserir(t);
		}
	}

	@Override
	public void remover(PontoInteresseLocal t) throws DAOException,
			SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM ponto_interesse_local"
				   + " WHERE cod_usuario = ?"
				   + "   AND num_ponto_local = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t.getUsuario().getCodigo());
		stm.setInt(2, t.getCodigo());

		stm.execute();
	}
	
	/**
	 * Metodo responv�l por excluir todos os registros de pontos locais do usuario informado
	 * @param PontoInteresseLocalResource t
	 * @throws DAOException
	 * @throws SQLException
	 */
	public void removeTodos(Usuario usuario) throws DAOException,	SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM ponto_interesse_local"
				   + " WHERE cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, usuario.getCodigo());

		stm.execute();
	}
	
	public PontoInteresseLocal retornaPontoInteresseLocal(Integer codUsuario, Integer numPonto) 
			throws DAOException,	SQLException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_usuario,"
						 + " num_ponto_local,"
						 + " nom_ponto_local,"
						 + " dsc_ponto_local,"
						 + " val_latitude,"
						 + " val_longitude"
				   + "  FROM ponto_interesse_local"
				   + " WHERE cod_usuario = ?"
				   + "   AND num_ponto_local = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, codUsuario);
		stm.setInt(1, numPonto);

		rs = stm.executeQuery();
		
		PontoInteresseLocal ponto = null;
		while(rs.next()){
			ponto = new PontoInteresseLocal();
			
			Integer codUser = rs.getInt("cod_usuario");
			Usuario user = UsuarioDAO.getInstance().retornaUsuario(codUser);
			ponto.setUsuario(user);
			
			ponto.setCodigo(rs.getInt("num_ponto_local"));
			ponto.setNome(rs.getString("nom_ponto_local"));
			ponto.setDescricao(rs.getString("dsc_ponto_local"));
			
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			Localizacao l = new Localizacao(lat, lon);
			ponto.setLocalizacao(l);
		}
		
		if(ponto == null){
			throw new DAOException("Ponto de Interesse Local n�o encontrado. Cod: " + codUsuario + " Num: " + numPonto, 1001);
		}
		
		return ponto;
	}
	
	public List<PontoInteresseLocal> retornaPontoInteresseLocal(Integer codUsuario) 
			throws DAOException,	SQLException{
		List<PontoInteresseLocal> pontos;
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		pontos = new ArrayList<PontoInteresseLocal>();
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_usuario,"
						 + " num_ponto_local,"
						 + " nom_ponto_local,"
						 + " dsc_ponto_local,"
						 + " val_latitude,"
						 + " val_longitude"
				   + "  FROM ponto_interesse_local"
				   + " WHERE cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, codUsuario);

		rs = stm.executeQuery();
		
		PontoInteresseLocal ponto = null;
		while(rs.next()){
			ponto = new PontoInteresseLocal();
			
			Integer codUser = rs.getInt("cod_usuario");
			Usuario user = UsuarioDAO.getInstance().retornaUsuario(codUser);
			ponto.setUsuario(user);
			
			ponto.setCodigo(rs.getInt("num_ponto_local"));
			ponto.setNome(rs.getString("nom_ponto_local"));
			ponto.setDescricao(rs.getString("dsc_ponto_local"));
			
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			Localizacao l = new Localizacao(lat, lon);
			ponto.setLocalizacao(l);
			
			pontos.add(ponto);
		}
		
		if(pontos.isEmpty()){
			throw new DAOException("Pontos de Interesse Local n�o encontrados. Cod: " + codUsuario, 1001);
		}
		
		return pontos;
	}

	@Override
	public List<PontoInteresseLocal> retornaTodos() throws DAOException,
			SQLException {
		List<PontoInteresseLocal> pontos = null;
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		pontos = new ArrayList<PontoInteresseLocal>();
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_usuario,"
						 + " num_ponto_local,"
						 + " nom_ponto_local,"
						 + " dsc_ponto_local,"
						 + " val_latitude,"
						 + " val_longitude"
				   + "  FROM ponto_interesse_local";

		stm = conn.prepareStatement(sql);

		rs = stm.executeQuery();
		
		PontoInteresseLocal ponto = null;
		while(rs.next()){
			ponto = new PontoInteresseLocal();
			Integer codUsuario = rs.getInt("cod_usuario");
			Usuario user = UsuarioDAO.getInstance().retornaUsuario(codUsuario);
			
			ponto.setUsuario(user);
			ponto.setCodigo(rs.getInt("num_ponto_local"));
			ponto.setNome(rs.getString("nom_ponto_local"));
			ponto.setDescricao(rs.getString("dsc_ponto_local"));
			
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			Localizacao l = new Localizacao(lat, lon);
			
			ponto.setLocalizacao(l);
			
			pontos.add(ponto);
		}
		
		if(pontos.isEmpty()){
			throw new DAOException("Todos Pontos de Interesse Local n�o encontrados.", 1001);
		}
		
		return pontos;
	}

	private Integer getMaxCod(Integer codigoUsuario) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select coalesce(max(num_ponto_local),0) + 1 as val "
				    + " FROM ponto_interesse_local"
				   + " where cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, codigoUsuario);
		
		rs = stm.executeQuery();
		
		Integer val = new Integer(0);
		if(rs.next()){
			val = rs.getInt("val");
		}
		return val;
	}
	
}
