/**
 * 
 */
package br.com.visedu.resources.pontointeresse;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.visedu.controller.pontointeresse.PontoInteresseLocalController;
import br.com.visedu.model.pontoInteresse.PontoInteresseLocal;
import br.com.visedu.resources.excecao.NoContentException;

import com.google.gson.Gson;
import com.sun.jersey.spi.resource.Singleton;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 13/08/2014
 * @version 1.0
 */
@Singleton
@Path("pontoLocal")
public class PontoInteresseLocalResource {

	@POST
	@Path("/inserir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String inserePontoLocal(PontoInteresseLocal ponto){
		boolean ret = new PontoInteresseLocalController().inserirPontoLocal(ponto);
		//System.out.println("Chamou inserePontoLocal!");
		if(ret){
			return "Ponto Local inserido com sucesso!"; 
		}else{
			return "Ocorreu erro na inser��o do Ponto Local";
		}
	}
	
	@GET
	@Path("/buscar/{codUsuario}/{numPonto}")
	@Produces(MediaType.APPLICATION_JSON)
	public PontoInteresseLocal getPontoLocal(@PathParam("codUsuario") int codUsuario,
										  @PathParam("numPonto") int numPonto){
		//System.out.println("Chamou getPontoLocal!");
		return new PontoInteresseLocalController().retornarPontoLocal(codUsuario, numPonto);
	}
	
	@GET
	@Path("/buscar/{codUsuario}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getListaPontos(@PathParam("codUsuario") int codUsuario){
		List<PontoInteresseLocal> lista = new PontoInteresseLocalController().retornarPontosLocais(codUsuario); 
		System.out.println("Chamou getLIstaPontos!");
		if(lista != null && !lista.isEmpty()){
			return new Gson().toJson(lista);
		}else{
			throw new NoContentException("Erro ao tentar listar o ponto! Lista vazia ou nulla");
		}
	}
	
	@GET
    @Path("/buscarTodos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PontoInteresseLocal> retornaTodos(){
		//System.out.println("Chamou retornaTodos!");
		return new PontoInteresseLocalController().retornarTodos();
    }

    @GET
    @Path("/buscarTodosGSON")
    @Produces(MediaType.APPLICATION_JSON)
    public String retornaTodosGSON(){
    	//System.out.println("Chamou retornaTodosGSON!");
    	return new Gson().toJson(new PontoInteresseLocalController().retornarTodos());
    }
    
    @POST
    @Path("/deletar")
    @Produces(MediaType.APPLICATION_JSON)
    public String excluirPontoLocal(PontoInteresseLocal ponto){
    	boolean ret = new PontoInteresseLocalController().removePontoLocal(ponto);
    	if(ret){
    		return "Ponto Exclu�do!";
    	}else{
    		throw new NoContentException("Erro ao tentar excluir o ponto!");
    	}
    }
	
}
