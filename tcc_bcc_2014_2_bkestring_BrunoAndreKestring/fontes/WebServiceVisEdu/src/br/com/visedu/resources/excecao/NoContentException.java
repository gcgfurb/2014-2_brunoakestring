/**
 * 
 */
package br.com.visedu.resources.excecao;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Classe responsavel por gerir os erros que ocorrem no retorno da solicita��o para o web service cliente 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 20/08/2014
 * @version 1.0
 */
public class NoContentException extends WebApplicationException {
    private static final long serialVersionUID = 1L;

    public NoContentException(String mensagem) {
     super(Response.status(Status.NOT_FOUND).entity(mensagem).build());
    }
}
