/**
 * 
 */
package br.com.visedu.controller.pontointeresse;

import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.dao.pontointeresse.PontoInteresseLocalDAO;
import br.com.visedu.model.pontoInteresse.PontoInteresseLocal;
import br.com.visedu.model.usuario.Usuario;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 12/08/2014
 * @version 1.0
 */
public class PontoInteresseLocalController {

	public PontoInteresseLocalController(){
		
	}
	
	public boolean inserirPontoLocal(PontoInteresseLocal ponto){
		try {
			PontoInteresseLocalDAO.getInstance().inserir(ponto);
			return true;
		}  catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao inserir Ponto Interesse Local.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public void atualizarPontoLocal(PontoInteresseLocal ponto){
		try {
			PontoInteresseLocalDAO.getInstance().atualizar(ponto);
		}  catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao atualizar Ponto Interesse Local.\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public boolean removePontoLocal(PontoInteresseLocal ponto){
		try {
			PontoInteresseLocalDAO.getInstance().remover(ponto);
			return true;
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao remover Ponto Interesse Local.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public void removeTodosPontoLocal(Usuario usuario){
		try {
			PontoInteresseLocalDAO.getInstance().removeTodos(usuario);
		}  catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao remover todos Pontos Interesse Locais.\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public PontoInteresseLocal retornarPontoLocal(Integer codUsuario, Integer numPonto){
		try {
			return PontoInteresseLocalDAO.getInstance().retornaPontoInteresseLocal(codUsuario, numPonto);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retornar Ponto Interesse Local.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<PontoInteresseLocal> retornarPontosLocais(Integer codUsuario){
		try {
			return PontoInteresseLocalDAO.getInstance().retornaPontoInteresseLocal(codUsuario);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retornar Pontos Interesse Locais.\n" + e.getMessage());
			return null;
		}
	}
	
	public List<PontoInteresseLocal> retornarTodos(){
		try {
			return PontoInteresseLocalDAO.getInstance().retornaTodos();
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retornar Todos Ponto Interesse Global.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}
