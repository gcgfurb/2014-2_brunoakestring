/**
 * 
 */
package br.com.visedu.controller.pontointeresse;

import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.dao.pontointeresse.PontoInteresseGlobalDAO;
import br.com.visedu.model.pontoInteresse.PontoInteresseGlobal;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 12/08/2014
 * @version 1.0
 */
public class PontoInteresseGlobalController {

	public PontoInteresseGlobalController(){
		
	}
	
	public boolean inserirPontoGlobal(PontoInteresseGlobal ponto){
		try {
			PontoInteresseGlobalDAO.getInstance().inserir(ponto);
			return true;
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao inserir Ponto Interesse Global.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public void atualizaPontoGlobal(PontoInteresseGlobal ponto){
		try {
			PontoInteresseGlobalDAO.getInstance().atualizar(ponto);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao atualizar Ponto Interesse Global.\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void removerPontoGlobal(PontoInteresseGlobal ponto){
		try {
			PontoInteresseGlobalDAO.getInstance().remover(ponto);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao remover Ponto Interesse Global.\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public PontoInteresseGlobal retornaPontoGlobal(Integer codPonto){
		try {
			return PontoInteresseGlobalDAO.getInstance().retornaPontoInteresseGlobal(codPonto);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retornar Ponto Interesse Global.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<PontoInteresseGlobal> retornarTodosPontoGlobal(){
		try {
			return PontoInteresseGlobalDAO.getInstance().retornaTodos();
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retornar todos Pontos Interesse Globais.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
}
