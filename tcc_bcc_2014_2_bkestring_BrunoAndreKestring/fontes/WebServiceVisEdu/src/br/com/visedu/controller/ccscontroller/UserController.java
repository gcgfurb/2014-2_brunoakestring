/**
 * 
 */
package br.com.visedu.controller.ccscontroller;

import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.ccsdao.UserDAO;
import br.com.visedu.model.ccsmodel.User;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 20/10/2014
 * @version 1.0
 */
public class UserController {

	public UserController(){
		
	}
	
	public User retornaById(String gcmId){
		return UserDAO.getInstance().retornaById(gcmId);
	}
	
	public void inserir(User user) throws SQLException{
		UserDAO.getInstance().inserir(user);
	}
	
	public void remover(User user) throws SQLException{
		UserDAO.getInstance().remover(user);
	}
	
	public List<User> retornaByNome(String name){
		return UserDAO.getInstance().retornaByNome(name);
	}
	
	public List<User> retornaTodos() throws SQLException{
		return UserDAO.getInstance().retornaTodos();
	}
	
}
