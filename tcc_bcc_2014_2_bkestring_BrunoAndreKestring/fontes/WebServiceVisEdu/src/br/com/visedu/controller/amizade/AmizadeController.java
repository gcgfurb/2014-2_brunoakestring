/**
 * 
 */
package br.com.visedu.controller.amizade;

import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.amizade.AmizadeDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.model.amizade.Amizade;

/**
 * Classe responsavel por realizar o controle da utiliza��o das classes AmizadeDAO
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 12/08/2014
 * @version 1.0
 */
public class AmizadeController {

	public AmizadeController(){
		
	}
	
	public boolean inserirAmizade(Amizade amizade){
		try {
			AmizadeDAO.getInstance().inserir(amizade);
			return true;
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao inserir amizade.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public void atualizarAmizade(Amizade amizade){
		try {
			AmizadeDAO.getInstance().atualizar(amizade);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao atualizar amizade.\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public boolean removerAmizade(Amizade amizade){
		try {
			AmizadeDAO.getInstance().remover(amizade);
			return true;
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao remover amizade.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public void removerTodasAmizades(Amizade amizade){
		try {
			AmizadeDAO.getInstance().removerTodos(amizade);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao remover todas amizades.\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public Amizade retornarAmizade(Integer codUsuario, Integer codAmigo){
		try {
			return AmizadeDAO.getInstance().retornarAmizade(codUsuario, codAmigo);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retornar amizade.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Amizade> retornarListaAmizade(Integer codUsuario){
		try {
			return AmizadeDAO.getInstance().retornarListaAmizades(codUsuario);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retonar lista de amizade.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Amizade> retornarAmigosLogados(Integer codUsuario){
		try {
			return AmizadeDAO.getInstance().retornarAmigosLogados(codUsuario);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao retonar lista de amizade.\n" + e.getMessage());
			return null;
		}
	}
	
	public List<Amizade> retornarTodos(){
		try {
			return AmizadeDAO.getInstance().retornaTodos();
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorre erro ao atualizar amizade.\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public void atualizaPermissao(Amizade amizade) throws SQLException{
		AmizadeDAO.getInstance().atualizaPermissao(amizade);
	}
}
