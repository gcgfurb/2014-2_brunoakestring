package br.com.viseduandroid;

import com.google.android.gms.common.GooglePlayServicesUtil;

import br.com.viseduandroid.R;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.map.MapaActivity;
import br.com.viseduandroid.webserviceclient.rest.usuario.UsuarioREST;
import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	protected ProgressDialog progress;
	private EditText email;
	private EditText senha;
	private Usuario usuario;
	private String erro;

	private Context context; 
			
//	private UiLifecycleHelper uiHelper;
//	private Session.StatusCallback callBack = new Session.StatusCallback() {
//
//		@Override
//		public void call(Session session, SessionState state,
//				Exception exception) {
//			onSessionStateChanged(session, state, exception);
//		}
//	};

	private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;

	public final static String MESSAGE = "LoginActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		usuario = null;
		
		this.context = getApplicationContext();

		email = (EditText) findViewById(R.id.editTextEmail);
		senha = (EditText) findViewById(R.id.editTextSenha);

		ConnectivityManager cm =
		        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		 
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
		                      activeNetwork.isConnectedOrConnecting();
		if(!isConnected){
			AlertDialog.Builder mensagem = new AlertDialog.Builder(this);
            mensagem.setTitle("Aten��o");
            mensagem.setMessage("N�o existe conex�o com internet. Ative-a!");
            mensagem.setNeutralButton("OK", null);
            mensagem.show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
//		Session.getActiveSession().onActivityResult(this, requestCode,
//				resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
//		uiHelper.onResume();

		// Verifica se a vers�o do google play est� atualizada
		GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	}

	private void showErrorDialog(int code) {
		GooglePlayServicesUtil.getErrorDialog(code, this,
				REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
	}

	public void login(View view) {
		this.progress = ProgressDialog.show(this, "Aguarde", "Autenticando...");
		final Context context = getApplicationContext();
		Thread t = new Thread() {
			public void run() {
				// criando inst�ncia de classe para executar um m�todo qualquer
				String sEmail = email.getText().toString();
				String sSenha = senha.getText().toString();
				UsuarioREST rest = new UsuarioREST();

				try {
					usuario = rest.validarSenha(sEmail, sSenha);
				} catch (Exception e) {
					e.printStackTrace();
					erro = e.getMessage();
					usuario = null;
				}
				progress.dismiss();
			}
		};
		t.start();
		try {
			t.join();
		} catch (Exception e) {
		}

		if (usuario == null) {
			Toast.makeText(MainActivity.this, erro, Toast.LENGTH_SHORT).show();
			return;
		}
//		else{
//			Toast.makeText(MainActivity.this, "Usuario = " + usuario.getNome(),
//					Toast.LENGTH_SHORT).show();
//		}
		acessaMapa();
	}

	private void acessaMapa() {
		Intent intent = new Intent(this, MapaActivity.class);
		if(usuario != null){
			intent.putExtra(MESSAGE, usuario);
		}
		startActivity(intent);
//		Intent intent = new Intent(this, GcmActivity.class);
//		startActivity(intent);
	}
	
	public void registra(View view){
		this.progress = ProgressDialog.show(this, "Aguarde", "Registrando...");
		final Context context = getApplicationContext();
		Thread t = new Thread() {
			public void run() {
				// criando inst�ncia de classe para executar um m�todo qualquer
				String sEmail = email.getText().toString();
				String sSenha = senha.getText().toString();
				UsuarioREST rest = new UsuarioREST();

				try {
					usuario = rest.registraUsuario(sEmail, sSenha);
				} catch (Exception e) {
					e.printStackTrace();
					erro = e.getMessage();
					usuario = null;
				}
				progress.dismiss();
			}
		};
		t.start();
		try {
			t.join();
		} catch (Exception e) {
		}
		if (usuario == null) {
			Toast.makeText(MainActivity.this, erro, Toast.LENGTH_SHORT).show();
			return;
		}
		acessaMapa();
	}

}
