/**
 * 
 */
package br.com.viseduandroid.view.map.listner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.map.MapaActivity;
import br.com.viseduandroid.view.ponto.PontoLocalActivity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 08/10/2014
 * @version 1.0
 */
public class SpinnerOnItemSelectedListner implements OnItemSelectedListener{

	private MapaActivity mapa;
	private Usuario usuario;
	
	public static final String MESSAGE_PONTOLOCAL = "SpinnerPontoLocal";
	public static final String MESSAGE = "SpinnerPontoLocalUsuario";
	
	private final static LatLng campus1 = new LatLng(-26.905529, -49.079021);
	private final static LatLng campus2 = new LatLng(-26.891350, -49.083793);
	private final static LatLng campus3 = new LatLng(-26.899156, -49.078432);
	
	public SpinnerOnItemSelectedListner(MapaActivity mapa, Usuario usuario) {
		this.mapa = mapa;
		this.usuario = usuario;
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, 
			int position, long id) {
		switch (position) {
			case 0:
				mapa.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(
				campus1, 16));
				break;
			case 1:
				mapa.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(
				campus2, 18));
				break;
			case 2:
				mapa.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(
				campus3, 18));
				break;
			case 3: 
				double[] latlng = {usuario.getLocalizacao().getLatitude(),
				usuario.getLocalizacao().getLongitude()};
				Intent intent = new Intent(mapa, PontoLocalActivity.class);
				intent.putExtra(MESSAGE_PONTOLOCAL, latlng);
				intent.putExtra(MESSAGE, this.usuario);
				mapa.startActivity(intent);
				break;
			default:
				break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		//Nao faz nada
	}

}
