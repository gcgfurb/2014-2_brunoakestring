package br.com.viseduandroid.view.ponto;

import br.com.viseduandroid.R;
import br.com.viseduandroid.model.localizacao.Localizacao;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseLocal;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.view.listapontos.ListaPontosLocaisActivity;
import br.com.viseduandroid.view.map.listner.MapLongClickListner;
import br.com.viseduandroid.view.map.listner.SpinnerOnItemSelectedListner;
import br.com.viseduandroid.webserviceclient.rest.pontointeresse.PontoInteresseLocalREST;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class PontoLocalActivity extends ActionBarActivity {

	private PontoInteresseLocal ponto;
	private double[] latlong;
	private Usuario usuario;
	private PontoInteresseLocalREST pontoRest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ponto_local);
		
		pontoRest = new PontoInteresseLocalREST();
		
		Intent intent = getIntent();
		//Verifica se esta acessando a atividade pela atividade ListaPontosLocais 
		if (intent != null && intent.hasExtra(ListaPontosLocaisActivity.MESSAGE)) {
			Log.i("TESTE", "Passou 1111");
			ponto = (PontoInteresseLocal) intent
					.getSerializableExtra(ListaPontosLocaisActivity.MESSAGE);
			usuario = (Usuario) intent.
					getSerializableExtra(ListaPontosLocaisActivity.MESSAGE_USUARIO);
			
		//Verifica se esta acessando a atividade pela atividade Mapa
		//atraves do evento de Click Longo
		}else if(intent != null && intent.hasExtra(MapLongClickListner.MESSAGE_PONTOLOCAL)){
			Log.i("TESTE", "Passou 2222");
			latlong = intent.getDoubleArrayExtra
					(MapLongClickListner.MESSAGE_PONTOLOCAL);
			usuario = (Usuario) intent.
					getSerializableExtra(MapLongClickListner.MESSAGE_PONTOLOCAL_USUARIO);
		//Verifica se esta acessando a atividade pela atividade Mapa
		//atraves do evento do Spinner
		}else if(intent != null && intent.hasExtra(SpinnerOnItemSelectedListner.MESSAGE_PONTOLOCAL)){
			Log.i("TESTE", "Passou 3333");
			latlong = intent.getDoubleArrayExtra
					(SpinnerOnItemSelectedListner.MESSAGE_PONTOLOCAL);
			usuario = (Usuario) intent.
					getSerializableExtra(SpinnerOnItemSelectedListner.MESSAGE);
		}
		
		Spinner spinner = (Spinner) findViewById(R.id.spinnerTipo);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.tipo_ponto, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, 
					int position, long id) {
				ImageView image = (ImageView) findViewById(R.id.imagePontoLocal);
				switch (position + 1) {
				case 1: //Biblioteca
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				case 2: //Sala de Aula
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				case 3: //Auditorio
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				case 4: //Laboratorio
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				case 5: //Cantina/Restaurante
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				case 6: //Banheiro
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				default://Personalizado
					image.setBackgroundResource(R.drawable.ic_stat_cloud2);
					break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		if(ponto != null){
			EditText edit = (EditText) findViewById(R.id.editTextNomePontoLocal);
			edit.setText(ponto.getNome());
			
			edit = (EditText) findViewById(R.id.editTextDescPontoLocal);
			edit.setText(ponto.getDescricao());
			
			edit = (EditText) findViewById(R.id.editTextLatitudePontoLocal);
			edit.setText(ponto.getLocalizacao().getLatitude().toString());
			
			edit = (EditText) findViewById(R.id.editTextLongitudePontoLocal);
			edit.setText(ponto.getLocalizacao().getLongitude().toString());
			
			spinner.setSelection(ponto.getTipo() -1);
			
			ImageView image = (ImageView) findViewById(R.id.imagePontoLocal);
	        
	        switch (ponto.getTipo()) {
			case 1: //Biblioteca
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 2: //Sala de Aula
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 3: //Auditorio
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 4: //Laboratorio
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 5: //Cantina/Restaurante
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			case 6: //Banheiro
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			default://Personalizado
				image.setBackgroundResource(R.drawable.ic_stat_cloud2);
				break;
			}
		}else if(latlong.length > 0){
			EditText edit = (EditText) findViewById(R.id.editTextNomePontoLocal);
			edit.requestFocus();
			
			edit = (EditText) findViewById(R.id.editTextLatitudePontoLocal);
			edit.setText(Double.toString(latlong[0]));
			
			edit = (EditText) findViewById(R.id.editTextLongitudePontoLocal);
			edit.setText(Double.toString(latlong[1]));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ponto_local, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}
	
	public void voltar(View view){
		finish();
	}
	
	public void confirmar(View view){
		double latitude = 0;
		double longitude = 0;
		if(ponto == null){
			ponto = new PontoInteresseLocal();
			ponto.setUsuario(usuario);
		}

		EditText edit = (EditText) findViewById(R.id.editTextNomePontoLocal);
		if(edit.getText().length() == 0){
			Toast.makeText(this, "Voc� deve preencher "
					+ "todos os campos", Toast.LENGTH_SHORT).show();
		}else{
			ponto.setNome(edit.getText().toString());
		}
		
		edit = (EditText) findViewById(R.id.editTextDescPontoLocal);

		if(edit.getText().length() == 0){
			Toast.makeText(this, "Voc� deve preencher "
					+ "todos os campos", Toast.LENGTH_SHORT).show();
		}else{
			ponto.setDescricao(edit.getText().toString());
		}
		
		edit = (EditText) findViewById(R.id.editTextLatitudePontoLocal);

		if(edit.getText().length() == 0){
			Toast.makeText(this, "Voc� deve preencher "
					+ "todos os campos", Toast.LENGTH_SHORT).show();
		}else{
			latitude = Double.parseDouble(edit.getText().toString());
		}
		
		edit = (EditText) findViewById(R.id.editTextLongitudePontoLocal);

		if(edit.getText().length() == 0){
			Toast.makeText(this, "Voc� deve preencher "
					+ "todos os campos", Toast.LENGTH_SHORT).show();
		}else{
			longitude = Double.parseDouble(edit.getText().toString());
		}
		
		Localizacao localizacao = new Localizacao(latitude, longitude);
		ponto.setLocalizacao(localizacao);
		
		Spinner spinner = (Spinner) findViewById(R.id.spinnerTipo);
		
		ponto.setTipo(spinner.getSelectedItemPosition() + 1);
		
		try{
			new PontoInteresseLocalREST().inserirPontoLocal(ponto);
		}catch(Exception e){
			e.printStackTrace();
			Log.d("ERRO", "Ocorreu erro ao inserir/atualizar ponto local");
		}
		finish();
	}
	
}
