package br.com.viseduandroid.webserviceclient.cliente;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionReleaseTrigger;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import br.com.viseduandroid.webserviceclient.httpclient.HttpClientSingleton;

/**
 * Created by Bruno on 18/08/2014.
 */
public class WebserviceCliente {

	public WebserviceCliente(){
		
	}
	
	private String toString(InputStream stream) throws IOException {
		byte[] bytes = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int lidos;
		while ((lidos = stream.read(bytes)) > 0) {
			baos.write(bytes, 0, lidos);
		}
		return new String(baos.toString());
	}

	public final String[] get(String url) {
		String result[] = new String[2];
		HttpGet httpGet = new HttpGet(url);
		HttpResponse response;
		try {
			response = HttpClientSingleton.getHttpClientInstance().execute(
					httpGet);
			HttpEntity entity = response.getEntity();
			
			if (entity != null) {
				result[0] = String.valueOf(response.getStatusLine()
						.getStatusCode());
				InputStream inputStream = entity.getContent();
				result[1] = toString(inputStream);
				inputStream.close();
				Log.i("get", "Result from post JsonGet : " + result[0] + " : "
						+ result[1]);
				
			}
		} catch (Exception e) {
			Log.e("NVGL", "Falha ao acessar Web Service", e);
			result[0] = "0";
			result[1] = "Falha de rede";
		}
		return result;
	}

	public final String[] post(String url, String json) {
		String result[] = new String[2];
		try {
			HttpPost httpPost = new HttpPost(new URI(url));
			httpPost.setHeader("Content-type", "application/json");
			StringEntity stringEntity = new StringEntity(json, "UTF-8");
			httpPost.setEntity(stringEntity);

			HttpResponse response;
			response = HttpClientSingleton.getHttpClientInstance().execute(
					httpPost);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				result[0] = String.valueOf(response.getStatusLine()
						.getStatusCode());
				InputStream instream = entity.getContent();
				result[1] = toString(instream);
				instream.close();
				Log.d("post", "Result from post JsonPost : " + result[0]
						+ " : " + result[1]);
			}
		} catch (Exception e) {
			Log.e("NGVL", "Falha ao acessar Web service", e);
			result[0] = "0";
			result[1] = "Falha de rede!";
		}
		return result;
	}

}
