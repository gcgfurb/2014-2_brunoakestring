package br.com.viseduandroid.webserviceclient.httpclient;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/**
 * Created by Bruno on 18/08/2014.
 */
public class HttpClientSingleton {

    private static final int JSON_CONNECTION_TIMEOUT = 3000;
    private static final int JSON_SOCKET_TIMEOUT = 5000;
    private static HttpClientSingleton instance;
    private HttpParams httpParams;
    private DefaultHttpClient httpClient;

    private void setTimeOut(HttpParams params){
        HttpConnectionParams.setConnectionTimeout(params, JSON_CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, JSON_SOCKET_TIMEOUT);
    }

    private HttpClientSingleton(){
        httpParams = new BasicHttpParams();
        setTimeOut(httpParams);
        httpClient = new DefaultHttpClient(httpParams);
    }

    public static DefaultHttpClient getHttpClientInstance(){
        if(instance == null){
            instance = new HttpClientSingleton();
        }
        return instance.httpClient;
    }
}
