/**
 * 
 */
package br.com.viseduandroid.webserviceclient.rest.user;

import br.com.viseduandroid.model.ccsmodel.User;
import br.com.viseduandroid.model.usuario.Usuario;
import br.com.viseduandroid.webserviceclient.cliente.WebserviceCliente;

import com.google.gson.Gson;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 13/11/2014
 * @version 1.0
 */
public class UserREST {

	//private static final String URL_WS = "http://187.85.122.85:8080/WebServiceVisEdu/user/";
	//private static final String URL_WS = "http://192.168.0.13:8080/WebServiceVisEdu/user/";
	private static final String URL_WS = "http://192.168.169.5:8080/WebServiceVisEdu/user/";
	//private static final String URL_WS =  "http://campeche.inf.furb.br:8094/FURB-Mobile/WebServiceVisEdu/user/";
	
    public Usuario getUsuario(String regId) throws Exception {
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscar/" + regId);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            Usuario usuario = gson.fromJson(resposta[1],Usuario.class);
            return usuario;
        }else{
            throw new Exception(resposta[1]);
        }
    }
	
    public String inserirUser(User user) throws Exception {
        Gson gson = new Gson();
        String userJson = gson.toJson(user);
        String resposta[] = new WebserviceCliente().post(URL_WS + "inserir", userJson);
        if(resposta[0].equals("200")){
            return resposta[1];
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public String deletarUser(User user){
    	Gson gson = new Gson();
        String userJson = gson.toJson(user);
        String[] resposta = new WebserviceCliente().post(URL_WS + "deletar/", userJson);
        return resposta[1];
    }
}
