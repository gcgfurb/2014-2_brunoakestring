package br.com.viseduandroid.webserviceclient.rest.pontointeresse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import br.com.viseduandroid.model.pontoInteresse.PontoInteresseGlobal;
import br.com.viseduandroid.webserviceclient.cliente.WebserviceCliente;

/**
 * Created by Bruno on 19/08/2014.
 */
public class PontoInteresseGlobalREST {

    //private static final String URL_WS = "http://192.168.16.1:8080/WebServiceVisEdu/pontoGlobal/";
    //private static final String URL_WS = "http://192.168.56.1:8080/WebServiceVisEdu/pontoGlobal/";
    //private static final String URL_WS = "http://187.85.122.85:8080/WebServiceVisEdu/pontoGlobal/";
	//private static final String URL_WS = "http://192.168.0.13:8080/WebServiceVisEdu/pontoGlobal/";
	private static final String URL_WS = "http://192.168.169.5:8080/WebServiceVisEdu/pontoGlobal/";
	//private static final String URL_WS =  "http://campeche.inf.furb.br:8094/FURB-Mobile/WebServiceVisEdu/pontoGlobal/";

    public PontoInteresseGlobal getPontoGlobal(int codPonto) throws Exception {
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscar/" + codPonto);
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            PontoInteresseGlobal ponto = gson.fromJson(resposta[1],PontoInteresseGlobal.class);
            return ponto;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public List<PontoInteresseGlobal> getTodosPontosGlobais() throws Exception {
        List<PontoInteresseGlobal> pontos;
        String[] resposta = new WebserviceCliente().get(URL_WS + "buscarTodosGSON/");
        if(resposta[0].equals("200")){
            Gson gson = new Gson();
            pontos = new ArrayList<PontoInteresseGlobal>();
            JsonParser jsonParser = new JsonParser();
            JsonArray array = jsonParser.parse(resposta[1]).getAsJsonArray();
            for (int i = 0; i < array.size(); i++){
                pontos.add(gson.fromJson(array.get(i), PontoInteresseGlobal.class));
            }
            return pontos;
        }else{
            throw new Exception(resposta[1]);
        }
    }

    public String inserirPontoGlobal(PontoInteresseGlobal ponto) throws Exception {
        Gson gson = new Gson();
        String pontoJson = gson.toJson(ponto);
        String resposta[] = new WebserviceCliente().post(URL_WS + "inserir", pontoJson);
        if(resposta[0].equals("200")){
            return resposta[1];
        }else{
            throw new Exception(resposta[1]);
        }
    }

    /*public String deletarPonto(PontoInteresseGlobal ponto){
        String[] resposta = new WebServiceCliente().get(URL_WS + "deletar/" +
                ponto.getCodigo());
        return resposta[1];
    }*/
}
