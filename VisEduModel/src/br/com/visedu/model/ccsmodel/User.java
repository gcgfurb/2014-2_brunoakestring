/**
 * 
 */
package br.com.visedu.model.ccsmodel;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe responsavel por
 * 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 15/10/2014
 * @version 1.0
 */
@XmlRootElement
public class User {

	// @DatabaseField(id = true)
	private String gcmId;
	// @DatabaseField(index = true)
	private String name;

	public User() {
	}

	public User(String name, String gcmId) {
		this.name = name;
		this.gcmId = gcmId;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name + " : " + gcmId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gcmId == null) ? 0 : gcmId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (gcmId == null) {
			if (other.gcmId != null)
				return false;
		} else if (!gcmId.equals(other.gcmId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
