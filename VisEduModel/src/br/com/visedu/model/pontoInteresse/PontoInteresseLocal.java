package br.com.visedu.model.pontoInteresse;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.visedu.model.usuario.Usuario;

@XmlRootElement
public class PontoInteresseLocal extends PontoInteresseGlobal implements
		Serializable {

	private Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = super.hashCode();
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PontoInteresseLocal other = (PontoInteresseLocal) obj;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario)) {
			return false;
		} else if (!super.equals(obj)) {
			return false;
		}
		return true;
	}

}
