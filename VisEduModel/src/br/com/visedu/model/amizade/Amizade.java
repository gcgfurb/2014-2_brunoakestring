package br.com.visedu.model.amizade;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.visedu.model.usuario.Usuario;

@XmlRootElement
public class Amizade implements Serializable{

	private Usuario usuario;
	private Usuario amigo;
	private boolean permicao; //Permite visualizar a posicao do usu�rio;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Usuario getAmigo() {
		return amigo;
	}
	public void setAmigo(Usuario amigo) {
		this.amigo = amigo;
	}
	public boolean isPermicao() {
		return permicao;
	}
	public void setPermicao(boolean permicao) {
		this.permicao = permicao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amigo == null) ? 0 : amigo.hashCode());
		result = prime * result + (permicao ? 1231 : 1237);
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Amizade other = (Amizade) obj;
		if (amigo == null) {
			if (other.amigo != null)
				return false;
		} else if (!amigo.equals(other.amigo))
			return false;
		if (permicao != other.permicao)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
	
}
