/**
 * 
 */
package br.com.visedu;

import java.util.List;

import br.com.visedu.controller.pontointeresse.PontoInteresseLocalController;
import br.com.visedu.controller.usuario.UsuarioController;
import br.com.visedu.model.localizacao.Localizacao;
import br.com.visedu.model.pontoInteresse.PontoInteresseLocal;
import br.com.visedu.model.usuario.Usuario;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 16/11/2014
 * @version 1.0
 */
public class Testando {

	public static void main(String[] args) {
		
		List<Usuario> usuarios = new UsuarioController().retornarTodosUsuarios();
		PontoInteresseLocalController controller = new PontoInteresseLocalController();
		for( Usuario u : usuarios){
			if(u.getCodigo() == 3){
				for(int i = 0; i < 20; i++){
					PontoInteresseLocal p = new PontoInteresseLocal();
					p.setUsuario(u);
					p.setTipo(1);
					p.setNome("Ponto " + (i + 1));
					Localizacao l = new Localizacao();
					l.setLatitude(-26.906196283808770d);
					l.setLongitude(-49.078624695539474);
					p.setLocalizacao(l);
					controller.inserirPontoLocal(p);
				}
			}
		}
	}
	
}
