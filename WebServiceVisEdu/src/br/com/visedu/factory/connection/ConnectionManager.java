package br.com.visedu.factory.connection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

	private static ConnectionManager conexcaoSingleton;
	private final String CONFIG_PATH = "C:\\Program Files\\Apache Software Foundation\\"
			+ "apache-tomcat-7.0.55\\wtpwebapps\\WebServiceVisEdu\\WEB-INF\\classes\\"
			+ "br\\com\\visedu\\properties\\config.properties";
    private String url;
    private String user;
    private String pass;
    private Connection conn;
    
    private ConnectionManager(){
    	
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception e){
            System.out.println("Classe nao encontrada!");
            System.exit(1);
        }
        
        Properties props = new Properties();
        FileInputStream in = null;
        File f = null;
        
        try {
        	f = new File(CONFIG_PATH);
            in = new FileInputStream(f);
            props.load(in);
        } catch (IOException ex) {
            System.out.println("Deu erro na recuperação das propriedades!");
        }
        try{
            this.url = props.getProperty("database");
            this.user = props.getProperty("db.user");
            this.pass = props.getProperty("db.password");
            
            conn = DriverManager.getConnection(this.url, this.user, this.pass); 
            
            System.out.println("Conexao realizada com Sucesso!");
        }catch ( SQLException e){
            System.out.println(e.getMessage());
        }
        
    }
    
    public static ConnectionManager getInstance(){
        if(conexcaoSingleton == null){
            conexcaoSingleton = new ConnectionManager();
        }
        return conexcaoSingleton;
    }
    
    public Connection getConexao(){
        return this.conn;
    }
    
    public void fechaConexao(Connection conn, PreparedStatement stm, ResultSet res){
    	try{
    		if(conn != null){
    			conn.close();
    		}
    		if(stm != null){
    			stm.close();
    		}
    		if(res != null){
    			res.close();
    		}
    	}catch(Exception ex){
    		System.out.println("Ocorreu Erro ao fechar a conexao com banco: " + url);
    	}
    }
    
    public static void main(String[] args) {
    	Connection con = ConnectionManager.getInstance().getConexao();
        try{
            PreparedStatement pre = con.prepareStatement("Select * from usuario");
            
            ResultSet result = pre.executeQuery();
            System.out.println("Testando");
            while(result.next()){
                System.out.println(result.getString("cod_usuario") + " - " 
                		+ result.getString("nom_usuario") + " - "
                		+ result.getString("dsc_email"));
                
            }
            
        }catch(Exception e){
            System.out.println(" Erro " + e.getMessage());
            e.printStackTrace();
        }
	}
	
}
