/**
 * 
 */
package br.com.visedu.dao.pontointeresse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.visedu.dao.IDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.factory.connection.ConnectionManager;
import br.com.visedu.model.localizacao.Localizacao;
import br.com.visedu.model.pontoInteresse.PontoInteresseGlobal;

/**
 * Classe responsavel por Implementar a logica DAO da classe PontoInteresseGlobal
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 10/08/2014
 * @version 1.0
 */
public class PontoInteresseGlobalDAO implements IDAO<PontoInteresseGlobal>{

	private static PontoInteresseGlobalDAO instance;
	
	private PontoInteresseGlobalDAO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Metodo responsavel por controlar o acesso a instancia da classe PontoInteresseGlobalDAO
	 * @return the instance
	 */
	public static PontoInteresseGlobalDAO getInstance() {
		if(instance == null){
			instance = new PontoInteresseGlobalDAO();
		}
		return instance;
	}

	@Override
	public void inserir(PontoInteresseGlobal t) throws DAOException,
			SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "INSERT INTO ponto_interesse_global("
				+ " cod_ponto, "
				+ " nom_ponto, "
				+ " dsc_ponto, "
				+ " val_latitude, "
				+ " val_longitude,"
				+ " ind_tipo_ponto) "
				+ " VALUES (?, ?, ?, ?, ?, ?)";

		stm = conn.prepareStatement(sql);
		
		// TODO atualizar o codigo do objeto com o metodo getMaxCod
		t.setCodigo(this.getMaxCod());
		
		stm.setInt(1, t.getCodigo());
		stm.setString(2, t.getNome());
		stm.setString(3, t.getDescricao());
		stm.setDouble(4, t.getLocalizacao().getLatitude());
		stm.setDouble(5, t.getLocalizacao().getLongitude());
		stm.setInt(6, t.getTipo());
		
		stm.execute();
	}

	@Override
	public void atualizar(PontoInteresseGlobal t) throws DAOException,
			SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "UPDATE ponto_interesse_global"
					 + " SET nom_ponto = ?, "
					 + " dsc_ponto = ?,"
					 + " val_latitude = ?,"
					 + " val_longitude = ?"
					 + " ind_tipo_ponto = ? "
				   + " WHERE cod_ponto = ?";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getNome());
		stm.setString(2, t.getDescricao());
		stm.setDouble(3, t.getLocalizacao().getLatitude());
		stm.setDouble(4, t.getLocalizacao().getLongitude());
		stm.setInt(5, t.getTipo());
		stm.setInt(6, t.getCodigo());

		stm.execute();
	}

	@Override
	public void remover(PontoInteresseGlobal t) throws DAOException,
			SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM ponto_interesse_global"
				   + " WHERE cod_ponto = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t.getCodigo());

		stm.execute();
	}
	
	public PontoInteresseGlobal retornaPontoInteresseGlobal(Integer p) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_ponto,"
						 + " nom_ponto,"
						 + " dsc_ponto,"
						 + " val_latitude,"
						 + " val_longitude,"
						 + " ind_tipo_ponto "
				    + " FROM ponto_interesse_global"
				   + " WHERE cod_ponto = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, p);

		rs = stm.executeQuery();
		
		PontoInteresseGlobal ponto = null;
		while(rs.next()){
			ponto = new PontoInteresseGlobal();
			ponto.setCodigo(rs.getInt("cod_ponto"));
			ponto.setNome(rs.getString("nom_ponto"));
			ponto.setDescricao(rs.getString("dsc_ponto"));
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			Localizacao l = new Localizacao(lat, lon);
			ponto.setLocalizacao(l);
			ponto.setTipo(rs.getInt("ind_tipo_ponto"));
		}
		
		if(ponto == null){
			throw new DAOException("Ponto de Interesse Global n�o encontrado. Cod: " + p, 1001);
		}
		
		return ponto;
	}

	@Override
	public List<PontoInteresseGlobal> retornaTodos() throws DAOException,
			SQLException {
		List<PontoInteresseGlobal> pontos = null;
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		pontos = new ArrayList<PontoInteresseGlobal>();
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_ponto,"
						 + " nom_ponto,"
						 + " dsc_ponto,"
						 + " val_latitude,"
						 + " val_longitude,"
						 + " ind_tipo_ponto"
				    + " FROM ponto_interesse_global";

		stm = conn.prepareStatement(sql);

		rs = stm.executeQuery();
		
		PontoInteresseGlobal ponto = null;
		while(rs.next()){
			ponto = new PontoInteresseGlobal();
			ponto.setCodigo(rs.getInt("cod_ponto"));
			ponto.setNome(rs.getString("nom_ponto"));
			ponto.setDescricao(rs.getString("dsc_ponto"));
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			Localizacao l = new Localizacao(lat, lon);
			ponto.setLocalizacao(l);
			ponto.setTipo(rs.getInt("ind_tipo_ponto"));
			
			pontos.add(ponto);
		}
		
		if(pontos.isEmpty()){
			throw new DAOException("Todos Pontos de Interesse Global n�o encontrados.", 1001);
		}
		
		return pontos;
	}

	private Integer getMaxCod() throws SQLException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select coalesce(max(cod_ponto),0) + 1 as val "
				    + " FROM ponto_interesse_global";

		stm = conn.prepareStatement(sql);

		rs = stm.executeQuery();
		
		Integer val = new Integer(0);
		if(rs.next()){
			val = rs.getInt("val");
		}
		return val;
	}
	
}
