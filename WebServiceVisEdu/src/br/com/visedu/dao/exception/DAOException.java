package br.com.visedu.dao.exception;

public class DAOException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int codigo;
    
    public DAOException(String message){
        super(message);
    }
    
    public DAOException(String message, int codigo){
        super(message);
        this.codigo = codigo;
    }
    
    @Override
    public String getMessage(){
        return this.codigo + " - " + super.getMessage();
    }
	
}
