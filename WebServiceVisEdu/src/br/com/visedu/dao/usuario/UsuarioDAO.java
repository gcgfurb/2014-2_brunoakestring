	package br.com.visedu.dao.usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.visedu.dao.IDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.factory.connection.ConnectionManager;
import br.com.visedu.model.localizacao.Localizacao;
import br.com.visedu.model.usuario.Usuario;

/**
 * 
 * Classe responsavel por Implementar a logica DAO da classe Usuario
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 10/08/2014
 * @version 1.0
 */
public class UsuarioDAO implements IDAO<Usuario> {

	private static UsuarioDAO instance;
	
	private UsuarioDAO() {
	}
	
	/**
	 * Metodo responsavel por controlar o acesso a instancia da classe UsuarioDAO
	 * @return the instance
	 */
	public static UsuarioDAO getInstance() {
		if(instance == null){
			instance = new UsuarioDAO();
		}
		return instance;
	}
	
	@Override
	public void inserir(Usuario t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "INSERT INTO usuario "
				+ " (cod_usuario," 
    		    + " nom_usuario,"
				+ " dsc_email,"
    		    + " val_latitude,"
				+ " val_longitude,"
				+ " val_senha)"
				+ " VALUES ( ?, ?, ?, ?, ?, ?)";

		stm = conn.prepareStatement(sql);
		
		t.setCodigo(this.getMaxCod());
		
		stm.setInt(1, t.getCodigo());
		stm.setString(2, t.getNome());
		stm.setString(3, t.getEmail());
		stm.setDouble(4, t.getLocalizacao().getLatitude());
		stm.setDouble(5, t.getLocalizacao().getLongitude());
		stm.setString(6, t.getSenha());
		
		stm.execute();
	}

	@Override
	public void atualizar(Usuario t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "UPDATE usuario " 
				     + " SET nom_usuario = ?," 
				         + " dsc_email = ?,"
				         + " val_latitude = ?," 
				         + " val_longitude = ?,"
				         + " val_senha = ?"
				   + " WHERE cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getNome());
		stm.setString(2, t.getEmail());
		stm.setDouble(3, t.getLocalizacao().getLatitude());
		stm.setDouble(4, t.getLocalizacao().getLongitude());
		stm.setString(5, t.getSenha());
		stm.setInt(6, t.getCodigo());

		stm.execute();
	}

	@Override
	public void remover(Usuario t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM usuario"
				   + " WHERE cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t.getCodigo());

		stm.execute();
	}

	public Usuario retornaUsuario(Integer t) throws DAOException, SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select cod_usuario,"
						 + " nom_usuario,"
						 + " dsc_email,"
						 + " val_latitude,"
						 + " val_longitude,"
						 + " val_senha"
				    + " FROM usuario"
				   + " WHERE cod_usuario = ?";

		stm = conn.prepareStatement(sql);

		stm.setInt(1, t);

		rs = stm.executeQuery();
		
		Usuario u = null;
		while(rs.next()){
			u = new Usuario();
			u.setCodigo(rs.getInt("cod_usuario"));
			u.setNome(rs.getString("nom_usuario"));
			u.setEmail(rs.getString("dsc_email"));
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			u.setSenha(rs.getString("val_senha"));
			Localizacao l = new Localizacao(lat, lon);
			u.setLocalizacao(l);
		}
		
		if(u == null){
			throw new DAOException("Usuario n�o encontrado. Cod: " + t, 1001);
		}
		
		return u;
	}

	@Override
	public List<Usuario> retornaTodos() throws DAOException, SQLException {
		List<Usuario> usuarios = null;
		Connection conn = null; 
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		usuarios = new ArrayList<Usuario>();
		conn = ConnectionManager.getInstance().getConexao();
		
		String sql = "select cod_usuario,"
						 + " nom_usuario,"
						 + " dsc_email,"
						 + " val_latitude,"
						 + " val_longitude,"
						 + " val_senha"
				    + " FROM usuario";
		
		stm = conn.prepareStatement(sql);
		
		rs = stm.executeQuery();
		
		Usuario u = null;
		while(rs.next()){
			u = new Usuario();
			u.setCodigo(rs.getInt("cod_usuario"));
			u.setNome(rs.getString("nom_usuario"));
			u.setEmail(rs.getString("dsc_email"));
			u.setSenha(rs.getString("val_senha"));
			
			Double lat = rs.getDouble("val_latitude");
			Double lon = rs.getDouble("val_longitude");
			Localizacao l = new Localizacao(lat, lon);
			u.setLocalizacao(l);
			
			usuarios.add(u);
		}
		
		if(usuarios.isEmpty()){
			throw new DAOException("N�o foi poss�vel encontrar todos os usu�rios.", 1002);
		}
		return usuarios;
	}

	private Integer getMaxCod() throws SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select coalesce(max(cod_usuario),0) + 1 as val "
				    + " FROM usuario";

		stm = conn.prepareStatement(sql);

		rs = stm.executeQuery();
		
		Integer val = new Integer(0);
		if(rs.next()){
			val = rs.getInt("val");
		}
		return val;
	}

	public Usuario retornaUsuarioByNome(String nome) throws DAOException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select * from usuario "
					+ "WHERE upper(nom_usuario) = upper(?) ";

		try {
			stm = conn.prepareStatement(sql);

			stm.setString(1, nome);
			
			rs = stm.executeQuery();
			
			if(rs.next()){
				usuario = new Usuario();
				usuario.setCodigo(rs.getInt("cod_usuario"));
				usuario.setNome(rs.getString("nom_usuario"));
				usuario.setEmail(rs.getString("dsc_email"));
				usuario.setSenha(rs.getString("val_senha"));
				Localizacao localizacao = new Localizacao(rs.getDouble("val_latitude"),
						rs.getDouble("val_longitude"));
				usuario.setLocalizacao(localizacao);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Ocorreu erro ao procurar por usuario pelo nome: " + nome, 1001);
		}
		return usuario;
	}
	
	public Usuario retornaUsuarioByEmail(String email) throws DAOException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select * from usuario "
					+ "WHERE upper(dsc_email) = upper(?) ";

		try {
			stm = conn.prepareStatement(sql);

			stm.setString(1, email);
			
			rs = stm.executeQuery();
			
			if(rs.next()){
				usuario = new Usuario();
				usuario.setCodigo(rs.getInt("cod_usuario"));
				usuario.setNome(rs.getString("nom_usuario"));
				usuario.setEmail(rs.getString("dsc_email"));
				usuario.setSenha(rs.getString("val_senha"));
				Localizacao localizacao = new Localizacao(rs.getDouble("val_latitude"),
						rs.getDouble("val_longitude"));
				usuario.setLocalizacao(localizacao);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Ocorreu erro ao procurar por usuario pelo nome: " + email, 1001);
		}
		return usuario;
	}

	/**
	 * Metodo responv�l por
	 * @param nome
	 * @param senha
	 * @return
	 */
	public Usuario validarSenha(String nome, String senha) throws DAOException {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select * from usuario "
				   + " WHERE dsc_email = ? "
				   + "   and val_senha = ?";

		try {
			stm = conn.prepareStatement(sql);
	
			stm.setString(1, nome);
			stm.setString(2, senha);
			
			rs = stm.executeQuery();
			
			if(rs.next()){
				usuario = new Usuario();
				usuario.setCodigo(rs.getInt("cod_usuario"));
				usuario.setNome(rs.getString("nom_usuario"));
				usuario.setEmail(rs.getString("dsc_email"));
				usuario.setSenha(rs.getString("val_senha"));
				Localizacao localizacao = new Localizacao(rs.getDouble("val_latitude"),
						rs.getDouble("val_longitude"));
				usuario.setLocalizacao(localizacao);
			}
			
			if(usuario != null){
				return usuario;
			}else{
				throw new DAOException("Ocorreu erro ao procurar a senha do usuario nome: " + nome + "\n", 1003);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Ocorreu erro ao procurar a senha do usuario nome: " + nome + "\n" + e.getMessage(), 1001);
		}
	}
	
	public Usuario registraUsuario(String nome, String senha) throws DAOException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select * from usuario "
				   + " WHERE dsc_email = ? ";

		try {
			stm = conn.prepareStatement(sql);
	
			stm.setString(1, nome);
			
			rs = stm.executeQuery();
			
			if(!rs.next()){
				
				usuario = new Usuario();
				usuario.setNome(nome);
				usuario.setEmail(nome);
				usuario.setSenha(senha);
				Localizacao localizacao = new Localizacao();
				localizacao.setLatitude(0d);
				localizacao.setLongitude(0d);
				usuario.setLocalizacao(localizacao);
				inserir(usuario);
				
			}else{
				throw new DAOException("J� existe usuario com este nome", 1003);
			}
			return usuario;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Ocorreu erro ao procurar a senha do usuario nome: " + nome + "\n" + e.getMessage(), 1001);
		}
	}
	
	public void updatePosicao(int codUsuario, 
			double latitude, double longitude) throws SQLException {
		Connection conn = null;
		PreparedStatement stm = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "update usuario "
				    + "  set val_latitude = ?, "
				    + "      val_longitude = ? "
				    + "where cod_usuario = ?";
		stm = conn.prepareStatement(sql);
		stm.setDouble(1, latitude);
		stm.setDouble(2, longitude);
		stm.setInt(3, codUsuario);
		stm.execute();
	}
	
	public List<Usuario> usuarioNaoAmigo(int codUsuario) throws DAOException{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		Usuario usuario = null;
		List<Usuario> lista = null;
		
		lista = new ArrayList<Usuario>();
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select * from usuario "
				   + " WHERE cod_usuario <> ? "
				   + "   AND usuario.cod_usuario not in ( "
				   + " 			select amizade.cod_amigo from amizade "
				   + "           where amizade.cod_usuario = ? ) ";

		try {
			stm = conn.prepareStatement(sql);
	
			stm.setInt(1, codUsuario);
			stm.setInt(2, codUsuario);
			
			rs = stm.executeQuery();
			
			while(rs.next()){
				
				usuario = new Usuario();
				usuario.setCodigo(rs.getInt("cod_usuario"));
				usuario.setNome(rs.getString("nom_usuario"));
				usuario.setEmail(rs.getString("dsc_email"));
				usuario.setSenha(rs.getString("val_senha"));
				Localizacao localizacao = new Localizacao(rs.getDouble("val_latitude"),
						rs.getDouble("val_longitude"));
				usuario.setLocalizacao(localizacao);
				lista.add(usuario);
			}
			if(lista.isEmpty()){
				throw new DAOException("N�o existem usu�rios n�o amigos", 1003);
			}
			return lista;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Ocorreu erro ao procurar a usuarios n�o amigos do usuario codigo: " + codUsuario + "\n" + e.getMessage(), 1001);
		}
	}
}

