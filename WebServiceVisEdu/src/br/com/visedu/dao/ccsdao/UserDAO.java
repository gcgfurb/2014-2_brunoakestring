/**
 * 
 */
package br.com.visedu.dao.ccsdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.visedu.dao.IDAO;
import br.com.visedu.factory.connection.ConnectionManager;
import br.com.visedu.model.ccsmodel.User;

/**
 * Classe responsavel por
 * 
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 15/10/2014
 * @version 1.0
 */
public class UserDAO implements IDAO<User> {

	private static UserDAO instance;

	private UserDAO() {
	}

	/**
	 * Metodo responsavel por controlar o acesso a instancia da classe
	 * 
	 * @return the instance
	 */
	public static UserDAO getInstance() {
		if (instance == null) {
			instance = new UserDAO();
		}
		return instance;
	}

	@Override
	public void inserir(User t) throws SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = 
				"INSERT INTO user("
				+ " gcm_id," 
				+ " nom_user)"
				+ " VALUES (?, ?)";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getGcmId());
		stm.setString(2, t.getName());

		stm.execute();
	}

	@Override
	public void atualizar(User t) throws SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "UPDATE user "
					 + " SET gcm_id = ?,"
					 + "     nom_user = ?"
				   + " WHERE gcm_id = ?";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getGcmId());
		stm.setString(2, t.getName());
		stm.setString(3, t.getGcmId());

		stm.execute();
	}

	@Override
	public void remover(User t) throws SQLException {
		Connection conn = null;
		PreparedStatement stm = null;

		conn = ConnectionManager.getInstance().getConexao();
		String sql = "DELETE FROM user"
				   + " WHERE gcm_id = ? ";

		stm = conn.prepareStatement(sql);

		stm.setString(1, t.getGcmId());

		stm.execute();
	}

	@Override
	public List<User> retornaTodos() throws SQLException {
		List<User> lista;
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		lista = new ArrayList<User>();
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select gcm_id, "
						 + " nom_user "
				    + " FROM user ";

		stm = conn.prepareStatement(sql);

		rs = stm.executeQuery();
		
		User u = null;
		while(rs.next()){
			u = new User();
			String id = rs.getString("gcm_id");
			String nomUser = rs.getString("nom_user");
			u.setGcmId(id);
			u.setName(nomUser);
			lista.add(u);
		}
		return lista;
	}
	
	public User retornaById(String gcmId) {
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select gcm_id, "
						 + " nom_user "
				    + " FROM user "
				   + " WHERE gcm_id = ?";

		try{
			stm = conn.prepareStatement(sql);
	
			stm.setString(1, gcmId);
	
			rs = stm.executeQuery();
			
			User u = null;
			while(rs.next()){
				u = new User();
				String id = rs.getString("gcm_id");
				String nomUser = rs.getString("nom_user");
				u.setGcmId(id);
				u.setName(nomUser);
			}
			return u;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Ocorreu erro ao buscar User\n" + e.getMessage());
			return null;
		}
	}
	
	public List<User> retornaByNome(String nome) {
		List<User> lista;
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		lista = new ArrayList<User>();
		conn = ConnectionManager.getInstance().getConexao();
		String sql = "select gcm_id, "
						 + " nom_user "
				    + " FROM user "
				   + " WHERE nom_user = ?";

		try{
			stm = conn.prepareStatement(sql);
	
			stm.setString(1, nome);
	
			rs = stm.executeQuery();
			
			User u = null;
			while(rs.next()){
				u = new User();
				String id = rs.getString("gcm_id");
				String nomUser = rs.getString("nom_user");
				u.setGcmId(id);
				u.setName(nomUser);
				lista.add(u);
			}
			return lista;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Ocorreu erro ao buscar User\n" + e.getMessage());
			return null;
		}
	}
}
