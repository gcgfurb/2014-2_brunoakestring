/**
 * 
 */
package br.com.visedu.resources.amizade;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.sun.jersey.spi.resource.Singleton;

import br.com.visedu.controller.amizade.AmizadeController;
import br.com.visedu.controller.pontointeresse.PontoInteresseLocalController;
import br.com.visedu.model.amizade.Amizade;
import br.com.visedu.model.pontoInteresse.PontoInteresseLocal;
import br.com.visedu.resources.excecao.NoContentException;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 13/08/2014
 * @version 1.0
 */
@Singleton
@Path("amizade")
public class AmizadeResource {

	public AmizadeResource(){
		
	}
	
	@POST
	@Path("/inserir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String insereAmizade(Amizade amizade){
		boolean ret = new AmizadeController().inserirAmizade(amizade);
		//System.out.println("Chamou insereAmizade!");
		if(ret){
			return "Amizade inserida com sucesso!";
		}else{
			return "N�o foi poss�vel inserir Amizade!";
		}
	}
	
	@GET
	@Path("/buscar/{idUsuario}/{idAmigo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Amizade getAmizade(@PathParam("idUsuario") int codUsuario,
							  @PathParam("idAmigo") int codAmigo){
		//System.out.println("Chamou getAmizade!");
		return new AmizadeController().retornarAmizade(codUsuario, codAmigo);
	}
	
	@GET
	@Path("/buscar/{idUsuario}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getListaAmizade(@PathParam("idUsuario") int codUsuario){
		List<Amizade> lista = new AmizadeController().retornarListaAmizade(codUsuario);
		if(lista != null && !lista.isEmpty()){
			return new Gson().toJson(lista);
		}else{
			System.out.println("getListaAmizade Enviando mensagem de erro!");
			throw new NoContentException("N�o possui amigos");
		}
	}
	
	@GET
	@Path("/buscarAmigosLogados/{idUsuario}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAmigosLogados(@PathParam("idUsuario") int codUsuario){
		System.out.println("Chamou getAmigosLogados!");
		List<Amizade> lista = new AmizadeController().retornarAmigosLogados(codUsuario); 
		if(lista != null && !lista.isEmpty()){
			return new Gson().toJson(lista);
		}else{
			//System.out.println("getAmigosLogados Enviando mensagem de erro!");
			throw new NoContentException("N�o possui amigos logados");
		}
	}
	
	@GET
    @Path("/buscarTodos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Amizade> retornaTodos(){
		//System.out.println("Chamou retornaTodos!");
		return new AmizadeController().retornarTodos();
    }

    @GET
    @Path("/buscarTodosGSON")
    @Produces(MediaType.APPLICATION_JSON)
    public String retornaTodosGSON(){
    	//System.out.println("Chamou retornaTodosGSON!");
    	return new Gson().toJson(new AmizadeController().retornarTodos());
    }
    
    @POST
    @Path("/deletar")
    @Produces(MediaType.APPLICATION_JSON)
    public String excluirAmizade(Amizade amizade){
    	boolean ret = new AmizadeController().removerAmizade(amizade);
    	if(ret){
    		return "Amizade Exclu�da!";
    	}else{
    		throw new NoContentException("Erro ao tentar excluir o amizade!");
    	}
    }
    
    @POST
    @Path("/atualizaPermissao")
    @Produces(MediaType.APPLICATION_JSON)
    public String atualizaPermissao(Amizade amizade){
    	try {
			new AmizadeController().atualizaPermissao(amizade);
			return "Amizade Atualizada";
		} catch (SQLException e) {
			e.printStackTrace();
			throw new NoContentException("Erro ao tentar atualizar amizade!");
		}
    }
    
}
