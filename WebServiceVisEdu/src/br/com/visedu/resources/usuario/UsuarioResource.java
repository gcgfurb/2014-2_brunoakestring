package br.com.visedu.resources.usuario;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.sun.jersey.spi.resource.Singleton;

import br.com.visedu.controller.usuario.UsuarioController;
import br.com.visedu.model.usuario.Usuario;
import br.com.visedu.resources.excecao.NoContentException;

/**
 * Classe responsavel por implementar a logica do re
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 12/08/2014
 * @version 1.0
 */
@Singleton
@Path("usuario")
public class UsuarioResource {

	public UsuarioResource(){
	}
	
	@POST
	@Path("/inserir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String insereUsuario(Usuario usuario){
		boolean ret = new UsuarioController().inserirUsuario(usuario);
		//System.out.println("Chamou innsereUsuario!");
		if(ret){
			return "Usuario inserido com sucesso!"; 
		}else{
			return "Ocorreu erro na inser��o do usu�rio";
		}
	}
	
	@GET
	@Path("buscar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario getUsuario(@PathParam("id") int id){
		//System.out.println("Chamou getUsuario!");
		Usuario usuario = new UsuarioController().retornarUsuario(id);
		if(usuario == null){
			throw new NoContentException("Usuario n�o encontrado!");
		}
		return usuario;
	}
	
	@GET
	@Path("buscarByNome/{nome}")
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario getUsuarioByNome(@PathParam("nome") String nome){
		//System.out.println("Chamou getUsuarioByNome!");
		Usuario usuario = new UsuarioController().retornarUsuarioByNome(nome);
		if(usuario == null){
			throw new NoContentException("Usuario n�o encontrado!");
		}
		return usuario;
	}
	
	@GET
    @Path("/buscarTodos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> retornaTodos(){
		//System.out.println("Chamou retornaTodos!");
		return new UsuarioController().retornarTodosUsuarios();
    }

    @GET
    @Path("/buscarTodosGSON")
    @Produces(MediaType.APPLICATION_JSON)
    public String retornaTodosGSON(){
    	//System.out.println("Chamou retornaTodosGSON!");
    	return new Gson().toJson(new UsuarioController().retornarTodosUsuarios());
    }
    
    @GET
    @Path("/validarUsuario/{nome}/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public String validarUsuario(@PathParam("nome") String nome,
    					    	 @PathParam("email") String email){
    	try {
    		//System.out.println("Chamou validarUsuario!");
    		new UsuarioController().validaNomeEmail(nome, email);
    		return "Usuario e Email corretos";
		} catch (Exception e) {
			System.out.println("Ocorreu erro ao validar Usuario: " + nome + "/" + email +  "\n" + e.getMessage());
			e.printStackTrace();
			return e.getMessage();
		}
    }

    @GET
    @Path("/registra/{nome}/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario registraUsuario(@PathParam("nome") String nome,
    					    	 @PathParam("email") String email){
    	try {
    		//System.out.println("Chamou registraUsuario!");
    		return new UsuarioController().registrarUsuario(nome, email);
		} catch (Exception e) {
			System.out.println("Ocorreu erro ao validar Usuario: " + nome + "/" + email +  "\n" + e.getMessage());
			e.printStackTrace();
			throw new NoContentException(e.getMessage());
		}
    }
    
    @GET
    @Path("/deletar/{codUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deletarUsuario(@PathParam("codUsuario") int codUsuario){
    	//System.out.println("Chamou deletarUsuario!");
    	UsuarioController controller = new UsuarioController();
    	Usuario u = controller.retornarUsuario(codUsuario);
    	boolean ret = new UsuarioController().removerUsuario(u);
    	if(ret){
    		return "Usuario excluido com sucesso!";
    	}else{
    		return "Nao foi possivel excluir o usuario";
    	}
    }
    
    @GET
    @Path("/validarSenha/{nome}/{senha}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario validarSenha(@PathParam("nome") String nome,
    					    	 @PathParam("senha") String senha){
    	//System.out.println("Chamou validarSenha!");
		Usuario usuario = new UsuarioController().validarSenha(nome, senha);
		System.out.println("Passou aqui!");
		if(usuario == null){
			throw new NoContentException("Usuario n�o encontrado!");
		}
		return usuario;
    }
    
    @GET
    @Path("/updatePosicao/{codUsuario}/{latitude}/{longitude}")
    @Produces(MediaType.APPLICATION_JSON)
    public String updatePosicao(@PathParam("codUsuario") int codUsuario,
    		@PathParam("latitude") double latitude,
    		@PathParam("longitude") double longitude){
    	System.out.println("Chamou updatePosicao!");
		try{
			new UsuarioController().updatePosicao(codUsuario, latitude, longitude);
			return "Posicao atualizada com sucesso!";
		}catch(Exception e){
			throw new NoContentException("Nao foi possivel atualizar a posicao!");
		}
    }
    
    
    @GET
    @Path("/usuarioNaoAmigo/{codUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public String usuarioNaoAmigo(@PathParam("codUsuario") int codUsuario){
		try{
			//System.out.println("Chamou usuarioNaoAmigo!");
			List<Usuario> lista = new UsuarioController().usuarioNaoAmigo(codUsuario);
			return new Gson().toJson(lista);
		}catch(Exception e){
			throw new NoContentException("Nao foi possivel achar usuarios n�o amigos!");
		}
    }
}
