/**
 * 
 */
package br.com.visedu.resources.gcmUser;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.visedu.controller.ccscontroller.UserController;
import br.com.visedu.model.ccsmodel.User;
import br.com.visedu.resources.excecao.NoContentException;

import com.sun.jersey.spi.resource.Singleton;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 13/11/2014
 * @version 1.0
 */
@Singleton
@Path("user")
public class UserResource {

	public UserResource(){
		
	}
	
	@POST
	@Path("/inserir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String insereUser(User user){
		try{
			new UserController().inserir(user);
			return "User inserido com sucesso!"; 
		}catch (SQLException e){
			throw new NoContentException("User n�o inserido!");
		}
	}
	
	@POST
	@Path("/remover")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String removeUser(User user){
		try{
			new UserController().remover(user);
			return "User removido com sucesso!"; 
		}catch(SQLException e){
			throw new NoContentException("User n�o removido!");
		}
	}
}
