/**
 * 
 */
package br.com.visedu.resources.pontointeresse;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.visedu.controller.pontointeresse.PontoInteresseGlobalController;
import br.com.visedu.model.pontoInteresse.PontoInteresseGlobal;
import br.com.visedu.resources.excecao.NoContentException;

import com.google.gson.Gson;
import com.sun.jersey.spi.resource.Singleton;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 13/08/2014
 * @version 1.0
 */
@Singleton
@Path("pontoGlobal")
public class PontoInteresseGlobalResource {

	@POST
	@Path("/inserir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String insereUsuario(PontoInteresseGlobal ponto){
		boolean ret = new PontoInteresseGlobalController().inserirPontoGlobal(ponto);
		//System.out.println("Chamou insereUsuario!");
		if(ret){
			return "Ponto Global inserido com sucesso!"; 
		}else{
			return "Ocorreu erro na inser��o do Ponto Global";
		}
	}
	
	@GET
	@Path("/buscar/{codPonto}")
	@Produces(MediaType.APPLICATION_JSON)
	public PontoInteresseGlobal getUsuario(@PathParam("codPonto") int codPonto){
		//System.out.println("Chamou getUsuario!");
		return new PontoInteresseGlobalController().retornaPontoGlobal(codPonto);
	}
	
	@GET
    @Path("/buscarTodos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PontoInteresseGlobal> retornaTodos(){
		return new PontoInteresseGlobalController().retornarTodosPontoGlobal();
    }

    @GET
    @Path("/buscarTodosGSON")
    @Produces(MediaType.APPLICATION_JSON)
    public String retornaTodosGSON(){
    	//System.out.println("Chamou retornaTodosGSON!");
    	List<PontoInteresseGlobal> lista = new PontoInteresseGlobalController().retornarTodosPontoGlobal(); 
    	if(lista != null && !lista.isEmpty()){
    		return new Gson().toJson(lista);
    	}else{
			throw new NoContentException("Erro ao tentar listar o ponto! Lista vazia ou nulla");
		}
    }
	
}
