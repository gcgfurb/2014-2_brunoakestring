/**
 * 
 */
package br.com.visedu.controller.ccscontroller;

import java.sql.SQLException;

import br.com.visedu.dao.ccsdao.NotificationKeyDAO;
import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.model.ccsmodel.NotificationKey;

/**
 * Classe responsavel por
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 20/10/2014
 * @version 1.0
 */
public class NotificationKeyController {

	public NotificationKeyController(){
		
	}
	
	public void inserir(NotificationKey key) throws DAOException, SQLException{
		try{
			NotificationKeyDAO.getInstance().inserir(key);
		}catch(DAOException e){
			e.printStackTrace();
		}
	}
	
	public void removeByNome(String name) throws SQLException{
		NotificationKeyDAO.getInstance().removeByNome(name);
	}
	
	public NotificationKey retornaByNome(String name) throws SQLException{
		return NotificationKeyDAO.getInstance().retornaByNome(name);
	}
}
