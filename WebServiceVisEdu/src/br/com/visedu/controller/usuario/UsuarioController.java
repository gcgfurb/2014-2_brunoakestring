/**
 * 
 */
package br.com.visedu.controller.usuario;

import java.sql.SQLException;
import java.util.List;

import br.com.visedu.dao.exception.DAOException;
import br.com.visedu.dao.usuario.UsuarioDAO;
import br.com.visedu.model.usuario.Usuario;

/**
 * Classe responsavel por realizar o controle da utiliza��o das classes UsuarioDAO
 * @author Bruno Andr� Kestring - kestringbak@gmail.com
 * @since 11/08/2014
 * @version 1.0
 */
public class UsuarioController {

	public UsuarioController(){
		
	}
	
	public boolean inserirUsuario(Usuario user){
		try {
			Usuario u = null;
			if(user != null && user.getCodigo() != null){
				u = UsuarioDAO.getInstance().retornaUsuario(user.getCodigo());
			}
			if(u != null){
				throw new DAOException("J� existe usu�rio com o c�digo: " + u.getCodigo(),1001);
			}
			if(validaNomeEmail(user.getNome(), user.getEmail())){
				UsuarioDAO.getInstance().inserir(user);
			}
		} catch (DAOException e){
			System.out.println("Ocorreu erro ao realizar inser��o do usu�rio.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		} catch( SQLException e) {
			System.out.println("Erro");
			e.printStackTrace();
			return false;
		} catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean validaNomeEmail(String nome, String email) throws Exception{
		try{
			Usuario u = UsuarioDAO.getInstance().retornaUsuarioByNome(nome);
			if(u != null){
				throw new Exception("Nome j� cadastrado.");
			}
			u = UsuarioDAO.getInstance().retornaUsuarioByEmail(email);
			if(u != null){
				throw new Exception("E-mail j� cadastrado.");
			}
			return true;
		}catch(DAOException e){
			System.out.println("Ocorre erro ao validar nome e/ou email.\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public void atualizarUsuario(Usuario usuario){
		try {
			UsuarioDAO.getInstance().atualizar(usuario);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorreu erro ao atualizar usu�rio!\n" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public boolean removerUsuario(Usuario usuario){
		try {
			UsuarioDAO.getInstance().remover(usuario);
			return true;
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorreu erro ao remover usu�rio!\n" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public Usuario retornarUsuario(Integer codigoUsuario){
		try {
			return UsuarioDAO.getInstance().retornaUsuario(codigoUsuario);
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorreu erro ao retornar usu�rio!\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Usuario> retornarTodosUsuarios(){
		try {
			return UsuarioDAO.getInstance().retornaTodos();
		} catch (DAOException | SQLException e) {
			System.out.println("Ocorreu erro ao retornar todos usu�rios!\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public Usuario retornarUsuarioByNome(String nome){
		try {
			return UsuarioDAO.getInstance().retornaUsuarioByNome(nome);
		} catch (DAOException e) {
			System.out.println("Ocorreu erro ao retornar usu�rio pelo nome!\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public Usuario retornarUsuarioByEmail(String email){
		try {
			return UsuarioDAO.getInstance().retornaUsuarioByEmail(email);
		} catch (DAOException e) {
			System.out.println("Ocorreu erro ao retornar usu�rio pelo email!\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public Usuario validarSenha(String nome, String senha){
		try {
			return UsuarioDAO.getInstance().validarSenha(nome, senha);
		} catch (DAOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Usuario registrarUsuario(String nome, String senha) throws Exception{
		try {
			Usuario usuario = UsuarioDAO.getInstance().
					retornaUsuarioByNome(nome);
			if(usuario == null){
				return UsuarioDAO.getInstance().registraUsuario(nome, senha);
			}else{
				throw new Exception("Usuario j� existe com este nome!");
			}
		} catch (DAOException e) {
			e.printStackTrace();
			throw new Exception("Ocorreu erro ao registrar usu�rio");
		}
	}
	
	public void updatePosicao(int codUsuario,double latitude, double longitude) throws Exception{
		UsuarioDAO.getInstance().updatePosicao(codUsuario, latitude, longitude);
	}
	
	public List<Usuario> usuarioNaoAmigo(int codUsuario) throws Exception{
		return UsuarioDAO.getInstance().usuarioNaoAmigo(codUsuario);
	}
}
